# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_entity do
    user_id 1
    group_id 1
    admin false
  end
end
