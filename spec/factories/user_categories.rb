# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_category do
    user_id 1
    category_id 1
    overall_score 1.5
    position 1
  end
end
