# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_question do
    user_id 1
    question_id 1
    score 1.5
  end
end
