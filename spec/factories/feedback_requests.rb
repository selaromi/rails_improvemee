# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :feedback_request do
    solicitor_id 1
    solicited_id 1
    event_id 1
    reply false
  end
end
