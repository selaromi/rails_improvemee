# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    name "MyString"
    description "MyString"
    start_date "2014-04-19 00:41:20"
    end_date "2014-04-19 00:41:20"
  end
end
