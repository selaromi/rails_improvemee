# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :feedback do
    feedback_request_id 1
    sender_id 1
    receiver_id 1
    event_id 1
    is_anonymous false
  end
end
