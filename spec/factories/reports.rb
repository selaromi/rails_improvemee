# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :report do
    feedback_id 1
    action_taken_at "2014-07-29 20:08:56"
  end
end
