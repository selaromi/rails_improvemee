# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :feedback_question_score do
    feedback_id 1
    question_id 1
    score 1
  end
end
