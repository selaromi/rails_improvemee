# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :entity do
    name "MyString"
    description "MyString"
    start_date "2014-07-29 20:05:01"
    end_date "2014-07-29 20:05:01"
    type ""
    owner 1
    preferred_categories "MyString"
  end
end
