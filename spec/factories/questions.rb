# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :question do
    category_id 1
    text "MyString"
    overall_score 1
  end
end
