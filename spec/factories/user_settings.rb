# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_setting, :class => 'UserSetting' do
    program_name "MyString"
    overall_position 1
    nickname "MyString"
    graduation_year 1
    user_id 1
  end
end
