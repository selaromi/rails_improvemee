""" check if instance was created and save """
Then(/^an "([^"]*)" should be present with the following$/) do |class_name,table|
  class_name.classify.constantize.where(table.rows_hash).present?.should be_truthy
end

Then(/^a "([^"]*)" should be present with the following$/) do |class_name,table|
  class_name.classify.constantize.where(table.rows_hash).present?.should be_truthy
end

And(/^a "([^"]*)" should be present for the user with email "([^"]*)" with the following$/) do |class_name,email,table|
  class_name.classify.constantize.where(table.rows_hash).where(user_id: User.find_by_email(email).id).present?.should be_truthy
end

Given (/^the following "(.*?)" exist$/) do |class_name,data|
  data_hashes = data.hashes
  data_hashes.each do |hash|
    hash['password_confirmation'] = hash['password'] if class_name.singularize.classify.class == User.class
    hash['owner'] = User.find(hash['owner']) if class_name.singularize.classify.class == Entity.class
    class_name.singularize.classify.constantize.create!(hash)
  end
  (class_name.singularize.classify.constantize).count.should == data_hashes.size
end

Given (/^the following "(.*?)" exist with owner with id "(.*?)"$/) do |class_name,owner,data|
  data_hashes = data.hashes
  data_hashes.each do |hash|
    @object = class_name.singularize.classify.constantize.create!(hash)
    @object.owner = User.find(owner)
    @object.save
  end
  (class_name.singularize.classify.constantize).count.should == data_hashes.size
end