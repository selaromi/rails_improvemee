

And(/^the user with email id "([^"]*)" has user settings with nickname "([^"]*)", graduation year "([^"]*)" and program name "([^"]*)"$/) do |email,nickname,graduation_year,program_name|
  UserSetting.create!(user_id:User.find_by_email(email).id,nickname:nickname,program_name:program_name,graduation_year:graduation_year,overall_position:0).should be_truthy
end

And(/^"([^"]*)" authentication token is "([^"]*)"$/) do |first_name,auth_token|
  @user = User.find_by_first_name(first_name)
  @user.authentication_token = auth_token
  @user.save
end