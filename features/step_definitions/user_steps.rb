Given /^"([^"]*)" is a user with role "([^"]*)", email id "([^"]*)" and password "([^"]*)"$/ do |full_name,role, email, password|
  first_name, last_name = full_name.split
  @user = User.create(email: email, password: password, password_confirmation: password, first_name: first_name.to_s, last_name: last_name.to_s, role: role)
end

And /^his authentication token is "([^"]*)"$/ do |auth_token|
  @user.authentication_token = auth_token
  @user.save!
end

And /^his role is "([^"]*)"$/ do |role|
  @user.role = role
  @user.save!
end


And /^the auth_token should be different from "([^"]*)"$/ do |auth_token|
  @user.reload
  @user.authentication_token.should_not == auth_token
end

And /^the auth_token should still be "([^"]*)"$/ do |auth_token|
  @user.reload
  @user.authentication_token.should == auth_token
end

Then /^the user with email "([^"]*)" should have "([^"]*)" as his authentication_token$/ do |email, token|
  JsonSpec.remember(token).should == User.where(email: email).first.authentication_token.to_json
end

Then /^the user with email "([^"]*)" should not have "([^"]*)" as his authentication_token$/ do |email, token|
  JsonSpec.remember(token).should_not == User.where(email: email).first.authentication_token.to_json
end

And /^his password should be "([^"]*)"$/ do |password|
  @user.reload
  @user.valid_password?(password).should be_truthy
end

Then(/^a user should be present with the following$/) do |table|
  User.where(table.rows_hash).present?.should be_truthy

end


Given "the following user exists" do |table|
  User.create!(table.rows_hash)
end



Then(/^there should not be any user with email "(.*?)"$/) do |email|
  User.where(email: email).first.should be_nil
end





And(/^the settings should be present with the following for the user with email "([^"]*)"$/) do |email,table|
  UserSetting.where(table.rows_hash).where(user_id: User.find_by_email(email).id).present?.should be_truthy
end


Then(/^a user_event should be present with the following for the user with email "([^"]*)"$/) do |email,table|
  UserEvent.where(table.rows_hash).where(user_id: User.find_by_email(email).id).present?.should be_truthy
end

And "the following feedback_requests exist" do |feedback_request_data|
  feedback_request_hashes = feedback_request_data.hashes
  feedback_request_hashes.each do |feedback_request_hash|
    FeedbackRequest.create!(feedback_request_hash)
  end
  FeedbackRequest.count.should == feedback_request_hashes.size
end