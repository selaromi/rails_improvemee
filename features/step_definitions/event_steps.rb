Then(/^an event should be present with the following$/) do |table|
  Event.where(table.rows_hash).present?.should be_truthy
end