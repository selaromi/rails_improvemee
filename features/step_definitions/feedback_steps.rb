Then(/^a category with the name "([^"]*)" exists with its questions$/) do |category_name|
  @category = Category.create(name:category_name,description:category_name)
  5.times do |index|
    Question.create!(category_id:@category.id,text:"Question #{index+1}")
  end
end

Given(/^the following user_questions exist$/) do |table|
  user_questions_hashes = table.hashes
  user_questions_hashes.each do |user_questions_hash|
    UserQuestion.create!(user_questions_hash)
  end
  UserQuestion.count.should == user_questions_hashes.size
end
Given(/^the following user_category exist/) do |table|
  user_category_hashes = table.hashes
  user_category_hashes.each do |user_category_hash|
    UserCategory.create!(user_category_hash)
  end
  UserCategory.count.should == user_category_hashes.size
end


Then(/^the user_question should exists with the following$/) do |table|
  data = table.hashes
  data.each do |user_question|
    UserQuestion.where(user_question).any?.should be_truthy
  end
end

Then(/^the user_category should exists with the following$/) do |table|
  data = table.hashes
  data.each do |user_category|
    UserCategory.where(user_category).any?.should be_truthy
  end
end


Given(/^the following feedback exist$/) do |table|
  feedback_hashes = table.hashes
  feedback_hashes.each do |feedback_hash|
    Feedback.create!(feedback_hash)
  end
  Feedback.count.should == feedback_hashes.size
  #debugger; 0
end