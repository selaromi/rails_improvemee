

Then(/^a feedback request for the user with email "([^"]*)" should be present with the following$/) do |email,table|
  FeedbackRequest.where(table.rows_hash).where(solicitor_id:User.find_by_email(email).id).present?.should be_truthy
end

Then(/^an event with the name "([^"]*)" exists$/) do |event_name|
  @event = Event.create(name:event_name,description:event_name,start_date:"2014-04-29 17:45:05.41316",end_date:"2014-04-29 17:45:05.41316")
end

Then(/^they are part of the event$/) do
  User.find(10).events << @event
  User.find(11).events << @event
  User.find(12).events << @event
  User.find(13).events << @event
  User.find(14).events << @event
end

Then(/^all but the user with id 10 are part of the event$/) do
  User.find(10).events.delete_all
  User.find(11).events << @event
  User.find(12).events << @event
  User.find(13).events << @event
  User.find(14).events << @event
end

Then(/^a feedback should be present with the following$/) do |table|
  Feedback.where(table.rows_hash).present?.should be_truthy
end

Then(/^I wait for the user_statistics jobs to finish$/) do
  #UserSetting.drain
end