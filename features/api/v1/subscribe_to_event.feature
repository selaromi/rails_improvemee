Feature: Event Su

  Background:
    Given I send and accept JSON

  Scenario: subscribe to an event
    Given "Adam" is a user with email id "user@cmu.edu" and password "password123"
    And "Adam" authentication token is "auth_token"
    And an event with the name "testEvent" exists
    When I send a POST request to "/api/event/subscribe" with the following:
    """
      {
        "user" : {
          "email": "user@cmu.edu",
          "auth_token": "auth_token"
        },
        "event" : {
          "id": "1"
        }
      }
    """
    Then the response status should be "201"
    And a user_event should be present with the following for the user with email "user@cmu.edu"
      |event_id|1|