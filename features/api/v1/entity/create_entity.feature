Feature: Feedback

  Background:
    Given I send and accept JSON

  Scenario: Successfully create entity (with students)
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
      |15|user6@cmu.edu |Sixth      |User       |test1234 |auth_token_623      |user|
    When I send a POST request to "/api/entity" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "entity" : {
          "name": "Test Entity",
          "description":"My Test entity",
          "start_date" :"2009-01-01",
          "end_date" : "2009-06-01",
          "entity_type" : "class"
        },
        "students" : "15"
      }
      """
    Then the response status should be "201"
    And the JSON response should be:
    """
        {
          "id": 6,
          "name": "Test Entity",
          "description": "My Test entity",
          "start_date": "2009-01-01T00:00:00.000Z",
          "end_date": "2009-06-01T00:00:00.000Z",
          "entity_type": "class",
          "users": [
              {
                  "id": 15,
                  "first_name": "Sixth",
                  "last_name": "User"
              }
          ],
          "events": []
        }

    """
    Then an "entity" should be present with the following
      |name|Test Entity|
      |description|My Test entity|
      |start_date|2009-01-01T00:00:00.00000|
      |end_date|2009-06-01T00:00:00.000Z|
      |entity_type|class|

  Scenario: Successfully create entity (without students)
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
      |15|user6@cmu.edu |Sixth      |User       |test1234 |auth_token_623      |user|
    When I send a POST request to "/api/entity" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "entity" : {
          "name": "Test Entity",
          "description":"My Test entity",
          "start_date" :"2009-01-01",
          "end_date" : "2009-06-01",
          "entity_type" : "class"
        }
      }
      """
    Then the response status should be "201"
    And the JSON response should be:
    """
        {
          "id": 1,
          "name": "Test Entity",
          "description": "My Test entity",
          "start_date": "2009-01-01T00:00:00.000Z",
          "end_date": "2009-06-01T00:00:00.000Z",
          "entity_type": "class",
          "users": [],
          "events": []
        }

    """
    Then an "entity" should be present with the following
      |name|Test Entity|
      |description|My Test entity|
      |start_date|2009-01-01T00:00:00.00000|
      |end_date|2009-06-01T00:00:00.000Z|
      |entity_type|class|

  Scenario: Error creating entity with empty entity
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
    When I send a POST request to "/api/entity" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "entity" : {}
      }
      """
    Then the response status should be "422"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
        {
          "name": [
            "can't be blank"
          ],
          "description": [
            "can't be blank"
          ],
          "start_date": [
            "can't be blank"
          ],
          "end_date": [
            "can't be blank"
          ],
          "entity_type": [
            "can't be blank"
          ]
        }

    """

  Scenario: Error creating entity without privileges
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |user|
    When I send a POST request to "/api/entity" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "entity" : {
          "name": "Test entity",
          "description":"My Test entity",
          "start_date" :"2014-04-29 17:45:05.41316",
          "end_date" : "2014-04-29 19:45:05.41316"
        }
      }
      """
    Then the response status should be "403"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
        {
          "errors": ["Insufficient privileges"]
        }

    """