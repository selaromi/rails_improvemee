Feature: Feedback

  Background:
    Given I send and accept JSON

  Scenario: Successfully update entity
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
    Given the following "entities" exist with owner with id "14"
      |id|name          |description|entity_type|start_date               |end_date                 |
      |1|Test          |Description|class      |2014-04-29 17:45:05.41316|2014-06-29 17:45:05.41316 |
    When I send a PUT request to "/api/entity" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "entity" : {
          "id" : 1,
          "name": "Test Entity",
          "description":"My Test entity",
          "start_date" :"2014-04-29 17:45:05.41316",
          "end_date" : "2014-04-29 19:45:05.41316"
        }
      }
      """
    Then the response status should be "200"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
        {
          "id": 1,
          "name": "Test Entity",
          "description": "My Test entity",
          "start_date": "2014-04-29T17:45:05.413Z",
          "end_date": "2014-04-29T19:45:05.413Z",
          "entity_type": "class",
          "users": [],
          "events": []
        }

    """
    Then an "entity" should be present with the following
      |id|1|
      |name|Test Entity|
      |description|My Test entity|
      |start_date|2014-04-29 17:45:05.41316|
      |end_date|2014-04-29 19:45:05.41316|
      |entity_type|class                 |
      |owner      |14                    |

  Scenario: Successfully update entity users to zero
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
      |16|user6@cmu.edu |Fifth      |User       |test1234 |auth_token_623      |user|
    Given the following "entities" exist with owner with id "14"
      |id|name          |description|entity_type|start_date               |end_date                 |
      |1|Test          |Description|class      |2014-04-29 17:45:05.41316|2014-06-29 17:45:05.41316 |
    Given the following "user_entities" exist
      |id|entity_id |user_id|
      |1|1          |16|
    When I send a PUT request to "/api/entity" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "entity" : {
          "id" : 1,
          "name": "Test Entity",
          "description":"My Test entity",
          "start_date" :"2014-04-29 17:45:05.41316",
          "end_date" : "2014-04-29 19:45:05.41316"
        },
        "students" : ""
      }
      """
    Then the response status should be "200"
    And the JSON response should be:
    """
        {
          "id": 1,
          "name": "Test Entity",
          "description": "My Test entity",
          "start_date": "2014-04-29T17:45:05.413Z",
          "end_date": "2014-04-29T19:45:05.413Z",
          "entity_type": "class",
          "users": [],
          "events": []
        }

    """
    Then an "entity" should be present with the following
      |id|1|
      |name|Test Entity|
      |description|My Test entity|
      |start_date|2014-04-29 17:45:05.41316|
      |end_date|2014-04-29 19:45:05.41316|
      |entity_type|class                 |
      |owner      |14                    |


  Scenario: Successfully update entity users
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
      |16|user6@cmu.edu |Fifth      |User       |test1234 |auth_token_623      |user|
      |17|user7@cmu.edu |Fifth      |User       |test1234 |auth_token_723      |user|
    Given the following "entities" exist with owner with id "14"
      |id|name          |description|entity_type|start_date               |end_date                 |
      |1|Test          |Description|class      |2014-04-29 17:45:05.41316|2014-06-29 17:45:05.41316 |
    Given the following "user_entities" exist
      |id|entity_id |user_id|
      |1|1          |16|
    When I send a PUT request to "/api/entity" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "entity" : {
          "id" : 1,
          "name": "Test Entity",
          "description":"My Test entity",
          "start_date" :"2014-04-29 17:45:05.41316",
          "end_date" : "2014-04-29 19:45:05.41316"
        },
        "students" : "17"
      }
      """
    Then the response status should be "200"
    And the JSON response should be:
    """
        {
          "id": 1,
          "name": "Test Entity",
          "description": "My Test entity",
          "start_date": "2014-04-29T17:45:05.413Z",
          "end_date": "2014-04-29T19:45:05.413Z",
          "entity_type": "class",
          "users": [
            {
                "id": 17,
                "first_name": "Fifth",
                "last_name": "User"
            }
            ],
          "events": []
        }

    """
    Then an "entity" should be present with the following
      |id|1|
      |name|Test Entity|
      |description|My Test entity|
      |start_date|2014-04-29 17:45:05.41316|
      |end_date|2014-04-29 19:45:05.41316|
      |entity_type|class                 |
      |owner      |14                    |

#  Scenario: Error creating entity with empty entity
#    Given the following "users" exist
#      |id|email           |first_name |last_name  |password |authentication_token|role |
#      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
#    When I send a POST request to "/api/entity" with the following:
#    """
#      {
#        "user" : {
#          "email": "user5@cmu.edu",
#          "auth_token": "auth_token_523"
#        },
#        "entity" : {}
#      }
#      """
#    Then the response status should be "422"
##    When I wait for the user_statistics jobs to finish
#    And the JSON response should be:
#    """
#        {
#          "name": [
#            "can't be blank"
#          ],
#          "description": [
#            "can't be blank"
#          ],
#          "end_date": [
#            "can't be blank"
#          ],
#          "start_date": [
#            "can't be blank"
#          ]
#        }
#
#    """
#
#  Scenario: Error creating entity without privileges
#    Given the following "users" exist
#      |id|email           |first_name |last_name  |password |authentication_token|role |
#      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |user|
#    When I send a POST request to "/api/entity" with the following:
#    """
#      {
#        "user" : {
#          "email": "user5@cmu.edu",
#          "auth_token": "auth_token_523"
#        },
#        "entity" : {
#          "name": "Test entity",
#          "description":"My Test entity",
#          "start_date" :"2014-04-29 17:45:05.41316",
#          "end_date" : "2014-04-29 19:45:05.41316"
#        }
#      }
#      """
#    Then the response status should be "403"
##    When I wait for the user_statistics jobs to finish
#    And the JSON response should be:
#    """
#        {
#          "errors": ["Insufficient privileges"]
#        }
#
#    """