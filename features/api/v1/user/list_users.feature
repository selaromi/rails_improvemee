#Feature: List Users
#
#    Background:
#      Given I send and accept JSON
#
#    Scenario: Successfully list users when logged in user is admin
#      Given the following users exist
#        |id|email           |first_name |last_name  |password |authentication_token|role |
#        |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
#        |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
#        |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
#        |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
#        |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
#      When I send a POST request to "/api/students/list" with the following:
#      """
#      {
#        "user" : {
#          "email": "user5@cmu.edu",
#          "auth_token": "auth_token_523"
#        }
#      }
#      """
#      And the JSON response should be:
#      """
#      {
#        "users": [
#          {
#            "email": "user1@cmu.edu",
#            "first_name": "First",
#            "last_name": "User"
#          },
#          {
#            "email": "user2@cmu.edu",
#            "first_name": "Second",
#            "last_name": "User"
#          },
#          {
#            "email": "user3@cmu.edu",
#            "first_name": "Third",
#            "last_name": "User"
#          },
#          {
#            "email": "user4@cmu.edu",
#            "first_name": "Fourth",
#            "last_name": "User"
#          },
#          {
#            "email": "user5@cmu.edu",
#            "first_name": "Fifth",
#            "last_name": "User"
#          }
#        ]
#      }
#      """
#      Then the response status should be "200"
#
#    Scenario: Logged in user is not admin
#      Given the following users exist
#        |id|email           |first_name |last_name  |password |authentication_token|role |
#        |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
#        |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
#        |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
#        |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
#        |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
#      When I send a POST request to "/api/students/list" with the following:
#      """
#      {
#        "user" : {
#          "email": "user1@cmu.edu",
#          "auth_token": "auth_token_123"
#        }
#      }
#      """
#      Then the response status should be "403"
#      And the JSON response should be:
#      """
#      {"errors" : ["Insufficient privileges"]}
#      """
#
#    Scenario: User is not authenticated
#      When I send a POST request to "/api/students/list" with the following:
#      """
#      {
#        "user" : {
#          "email": "usesr5@cmu.edu",
#          "auth_token": "auth_tssoken_523"
#        }
#      }
#      """
#      Then the response status should be "401"
#      And the JSON response should be:
#      """
#      { "errors": ["Invalid login"] }
#      """
