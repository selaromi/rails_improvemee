Feature: User Settings

  Background:
    Given I send and accept JSON

  Scenario: Successful update user settings
    Given "Adam" is a user with email id "user@cmu.edu" and password "password123"
    And the user with email id "user@cmu.edu" has user settings with nickname "Adammy", graduation year "2004" and program name "summer 67"
    And "Adam" authentication token is "auth_token"
    When I send a PUT request to "/api/student/settings" with the following:
    """
      {
        "user" : {
          "email": "user@cmu.edu",
          "auth_token": "auth_token"
        },
        "user_setting" : {
          "program_name": "Summer 99",
          "nickname": "Mr Adams",
          "graduation_year": "2004"
        }
      }
      """
    Then the response status should be "200"
    And the JSON response at "program_name" should be "Summer 99"
    And the JSON response at "nickname" should be "Mr Adams"
    And the JSON response at "graduation_year" should be 2004

  Scenario: Unsuccessful update user settings
    Given "Adam" is a user with email id "user@cmu.edu" and password "password123"
    And the user with email id "user@cmu.edu" has user settings with nickname "Adammy", graduation year "2004" and program name "summer 67"
    And "Adam" authentication token is "auth_token"
    When I send a PUT request to "/api/student/settings" with the following:
    """
      {
        "user" : {
          "email": "uses5@cmu.edu",
          "auth_token": "NOT_auth_token"
        },
        "user_setting" : {
          "program_name": "Summer 99",
          "nickname": "Mr Adams",
          "graduation_year": "2004"
        }
      }
      """
    Then the response status should be "401"
    And the JSON response should be:
    """
      {"errors" : ["Invalid login"]}
    """