Feature: Feedback

  Background:
    Given I send and accept JSON

  Scenario: Successfully create event (with Entity without users)
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
    Given the following "entities" exist with owner with id "14"
      |id|name          |description|entity_type|start_date               |end_date                 |
      |1|Test          |Description|class      |2014-04-29 17:45:05.41316|2014-06-29 17:45:05.41316 |
    When I send a POST request to "/api/event" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "event" : {
          "name": "Test Event",
          "description":"My Test Event",
          "start_date" :"2014-04-29 17:45:05.41316",
          "end_date" : "2014-04-29 19:45:05.41316"
        },
        "entity" : "1"
      }
      """
    Then the response status should be "201"
    And the JSON response should be:
    """
        {
          "id": 4,
          "name": "Test Event",
          "description": "My Test Event",
          "start_date": "2014-04-29T17:45:05.413Z",
          "end_date": "2014-04-29T19:45:05.413Z",
          "created_at": "2014-07-28T19:35:27.653Z",
          "updated_at": "2014-07-28T19:35:27.653Z",
          "owner": null,
          "parent_entity": {
            "description": "Description",
            "end_date": "2014-06-29T17:45:05.413Z",
            "entity_type": "class",
            "name": "Test",
            "start_date": "2014-04-29T17:45:05.413Z"
          },
          "users":[]
        }

    """
    Then an event should be present with the following
      |name|Test Event|
      |description|My Test Event|
      |start_date|2014-04-29 17:45:05.41316|
      |end_date|2014-04-29 19:45:05.41316|

  Scenario: Error creating event with empty event
    Given the following "users" exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |organizer|
    Given the following "entities" exist with owner with id "14"
      |id|name          |description|entity_type|start_date               |end_date                 |
      |1|Test          |Description|class      |2014-04-29 17:4505.41316|2014-06-29 17:45:05.41316 |
    When I send a POST request to "/api/event" with the following:
    """
      {
        "user" : {
          "email": "user5@cmu.edu",
          "auth_token": "auth_token_523"
        },
        "event" : {},
        "entity" : "1"
      }
      """
    Then the response status should be "422"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
        {
          "name": [
            "can't be blank"
          ],
          "description": [
            "can't be blank"
          ],
          "end_date": [
            "can't be blank"
          ],
          "start_date": [
            "can't be blank"
          ]
        }

    """