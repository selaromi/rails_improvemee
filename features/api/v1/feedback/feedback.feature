Feature: Feedback

  Background:
    Given I send and accept JSON

  Scenario: Successfully send feedback (without previous existing feedback)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback_request": {
          "id": 14,
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |feedback_request_id|14|
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments|comment about the feedback|
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |4|
      |11       |2            |3|
      |11       |3            |2|
      |11       |4            |5|
      |11       |5            |4|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |11       |1            |3.60| 1 |

  Scenario: Successfully send feedback w/out comments (without previous existing feedback)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "",
        "feedback_request": {
          "id": 14,
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
    ##When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |feedback_request_id|14|
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments||
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |4|
      |11       |2            |3|
      |11       |3            |2|
      |11       |4            |5|
      |11       |5            |4|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |11       |1            |3.60| 1 |

  Scenario: Successfully send feedback (with previous existing feedback)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    And a category with the name "Recruiting Performance 2" exists with its questions
    Given the following user_questions exist
      |user_id  |question_id  | score |
      |11       |1            |1|
      |11       |2            |1|
      |11       |3            |1|
      |11       |4            |1|
      |11       |5            |1|
    Given the following user_category exist
      |user_id  |category_id  | overall_score | position | created_at |
      |10       |1            |4.40| 2 |Sun, 20 Apr 2014|
      |11       |1            |5.00| 1 |Sun, 20 Apr 2014|
      |12       |1            |3.60| 3 |Sun, 20 Apr 2014|
      |13       |1            |2.60| 4 |Sun, 20 Apr 2014|
      |14       |1            |0.60| 5 |Sun, 20 Apr 2014|
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback_request": {
          "id": 14,
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
    #When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |feedback_request_id|14|
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments|comment about the feedback|
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |2.50|
      |11       |2            |2.00|
      |11       |3            |1.50|
      |11       |4            |3.00|
      |11       |5            |2.50|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |10       |1            |4.40| 2 |
      |10       |1            |4.40| 1 |
      |11       |1            |5.00| 1 |
      |11       |1            |4.30| 2 |
      |12       |1            |3.60| 3 |
      |13       |1            |2.60| 4 |
      |14       |1            |0.60| 5 |

  Scenario: Successfully send feedback w/out comments (with previous existing feedback)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    And a category with the name "Recruiting Performance 2" exists with its questions
    Given the following user_questions exist
      |user_id  |question_id  | score |
      |11       |1            |1|
      |11       |2            |1|
      |11       |3            |1|
      |11       |4            |1|
      |11       |5            |1|
    Given the following user_category exist
      |user_id  |category_id  | overall_score | position | created_at |
      |10       |1            |4.40| 2 |Sun, 20 Apr 2014|
      |11       |1            |5.00| 1 |Sun, 20 Apr 2014|
      |12       |1            |3.60| 3 |Sun, 20 Apr 2014|
      |13       |1            |2.60| 4 |Sun, 20 Apr 2014|
      |14       |1            |0.60| 5 |Sun, 20 Apr 2014|
      |11       |2            |10.00| 1 |Sun, 20 Apr 2014|
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "",
        "feedback_request": {
          "id": 14,
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |feedback_request_id|14|
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments||
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |2.50|
      |11       |2            |2.00|
      |11       |3            |1.50|
      |11       |4            |3.00|
      |11       |5            |2.50|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |10       |1            |4.40| 2 |
      |10       |1            |4.40| 1 |
      |11       |1            |5.00| 1 |
      |11       |1            |4.30| 2 |
      |12       |1            |3.60| 3 |
      |13       |1            |2.60| 4 |
      |14       |1            |0.60| 5 |

  Scenario: Unsuccessfully send feedback (No feedback_request id)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback_request": {
          "id": "",
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "422"
    And the JSON response should be:
    """
      {
        "errors": ["No feedback request selected"]
      }
    """

  Scenario: Unsuccessfully send feedback (Feedback request does not belong to user)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback_request": {
          "id": 13,
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "403"
    And the JSON response should be:
    """
      {
        "errors": ["Insufficient Priviledges"]
      }
    """

  Scenario: Unsuccessfully send feedback (Not all questions were answered)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback_request": {
          "id": 14,
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,4"
      }
      """
    Then the response status should be "422"
    And the JSON response should be:
    """
      {
        "errors": ["all questions must be answered"]
      }
    """

  Scenario: Unsuccessfully send feedback (Feedback already given)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | true |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback_request": {
          "id": 14,
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,4,5"
      }
      """
    Then the response status should be "422"
    And the JSON response should be:
    """
      {
        "errors": ["Feedback already given"]
      }
    """

  Scenario: Successfully send feedback (without previous existing feedback && no feedback_request)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback": {
          "receiver_id": "11",
          "category_id":"1",
          "event_id":"1",
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments|comment about the feedback|
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |4|
      |11       |2            |3|
      |11       |3            |2|
      |11       |4            |5|
      |11       |5            |4|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |11       |1            |3.60| 1 |

  Scenario: Successfully send feedback w/out comments (without previous existing feedback && no feedback request)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "",
        "feedback": {
          "receiver_id": "11",
          "category_id":"1",
          "event_id":"1",
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
##When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments||
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |4|
      |11       |2            |3|
      |11       |3            |2|
      |11       |4            |5|
      |11       |5            |4|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |11       |1            |3.60| 1 |


  Scenario: Successfully send feedback (with previous existing feedback && no feedback request)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    And a category with the name "Recruiting Performance 2" exists with its questions
    Given the following user_questions exist
      |user_id  |question_id  | score |
      |11       |1            |1|
      |11       |2            |1|
      |11       |3            |1|
      |11       |4            |1|
      |11       |5            |1|
    Given the following user_category exist
      |user_id  |category_id  | overall_score | position | created_at |
      |10       |1            |4.40| 2 |Sun, 20 Apr 2014|
      |11       |1            |5.00| 1 |Sun, 20 Apr 2014|
      |12       |1            |3.60| 3 |Sun, 20 Apr 2014|
      |13       |1            |2.60| 4 |Sun, 20 Apr 2014|
      |14       |1            |0.60| 5 |Sun, 20 Apr 2014|
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "comment about the feedback",
        "feedback": {
          "receiver_id": "11",
          "category_id":"1",
          "event_id":"1",
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
    #When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments|comment about the feedback|
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |2.50|
      |11       |2            |2.00|
      |11       |3            |1.50|
      |11       |4            |3.00|
      |11       |5            |2.50|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |10       |1            |4.40| 2 |
      |10       |1            |4.40| 1 |
      |11       |1            |5.00| 1 |
      |11       |1            |4.30| 2 |
      |12       |1            |3.60| 3 |
      |13       |1            |2.60| 4 |
      |14       |1            |0.60| 5 |

  Scenario: Successfully send feedback w/out comments (with previous existing feedback && no feedback request)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    And a category with the name "Recruiting Performance 2" exists with its questions
    Given the following user_questions exist
      |user_id  |question_id  | score |
      |11       |1            |1|
      |11       |2            |1|
      |11       |3            |1|
      |11       |4            |1|
      |11       |5            |1|
    Given the following user_category exist
      |user_id  |category_id  | overall_score | position | created_at |
      |10       |1            |4.40| 2 |Sun, 20 Apr 2014|
      |11       |1            |5.00| 1 |Sun, 20 Apr 2014|
      |12       |1            |3.60| 3 |Sun, 20 Apr 2014|
      |13       |1            |2.60| 4 |Sun, 20 Apr 2014|
      |14       |1            |0.60| 5 |Sun, 20 Apr 2014|
      |11       |2            |10.00| 1 |Sun, 20 Apr 2014|
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "",
        "feedback": {
          "receiver_id": "11",
          "category_id":"1",
          "event_id":"1",
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
      """
    Then the response status should be "201"
#    When I wait for the user_statistics jobs to finish
    And the JSON response should be:
    """
      {
        "feedbacks": [
          {
            "question_id": 1,
            "score": 4
          },
          {
            "question_id": 2,
            "score": 3
          },
          {
            "question_id": 3,
            "score": 2
          },
          {
            "question_id": 4,
            "score": 5
          },
          {
            "question_id": 5,
            "score": 4
          }
        ]
      }
    """
    Then a feedback should be present with the following
      |receiver_id|11|
      |sender_id|10|
      |event_id|1|
      |is_anonymous|false|
      |comments||
      |seen|false|
    Then the user_question should exists with the following
      |user_id  |question_id  | score |
      |11       |1            |2.50|
      |11       |2            |2.00|
      |11       |3            |1.50|
      |11       |4            |3.00|
      |11       |5            |2.50|
    Then the user_category should exists with the following
      |user_id  |category_id  | overall_score | position |
      |10       |1            |4.40| 2 |
      |10       |1            |4.40| 1 |
      |11       |1            |5.00| 1 |
      |11       |1            |4.30| 2 |
      |12       |1            |3.60| 3 |
      |13       |1            |2.60| 4 |
      |14       |1            |0.60| 5 |


  Scenario: Unsuccessfully send feedback (Not all questions were answered)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | false |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "",
        "feedback": {
          "receiver_id": "11",
          "category_id":"1",
          "event_id":"1",
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,5,4"
      }
    """
    Then the response status should be "422"
    And the JSON response should be:
    """
    {
    "errors": ["all questions must be answered"]
    }
    """


  Scenario: Unsuccessfully send feedback (Feedback already given)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | true |
    Given the following feedback exist
      |event_id           |category_id |receiver_id  |sender_id |
      |1 |1      |10       |11 |
      |1 |1     |10       |12 |
      |1 |1      |10       |13 |
      |1 |1     |10       |14 |
      |1 |1      |11       |10 |
    And a category with the name "Recruiting Performance" exists with its questions
    When I send a POST request to "/api/feedback" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "feedback_comments": "",
        "feedback": {
          "receiver_id": "11",
          "category_id":"1",
          "event_id":"1",
          "is_anonymous":false
        },
        "feedback_question_score": "4,3,2,5,4"
      }
    """
    Then the response status should be "422"
    And the JSON response should be:
    """
      {
        "errors": ["Feedback already given"]
      }
    """