Feature: Event Users

  Background:
    Given I send and accept JSON

  Scenario: Successfully Get users for an event
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    And an event with the name "testEvent" exists
    And they are part of the event
    When I send a POST request to "/api/event/students/list" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "event" : {
          "id": "1"
        },
        "category" : {
          "id": 1
        }
      }
      """
    And the JSON response should be:
    """
      {
        "events": [
          {
            "id": "11",
            "email": "user2@cmu.edu",
            "first_name": "Second",
            "last_name": "User"
          },
          {
            "id": "12",
            "email": "user3@cmu.edu",
            "first_name": "Third",
            "last_name": "User"
          },
          {
            "id": "13",
            "email": "user4@cmu.edu",
            "first_name": "Fourth",
            "last_name": "User"
          },
          {
            "id": "14",
            "email": "user5@cmu.edu",
            "first_name": "Fifth",
            "last_name": "User"
          }
        ]
      }
      """
    Then the response status should be "200"

  Scenario: Unsuccessfully Get users for an event (no event id)
    Given the following users exist
      |id|email         |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    And an event with the name "testEvent" exists
    And they are part of the event
    When I send a POST request to "/api/event/students/list" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "event" : {
          "id": ""
        }
      }
      """
    And the JSON response should be:
    """
      {"errors": ["No event selected"]}
     """
    Then the response status should be "422"

  Scenario: Unsuccessfully Get users for an event (no event id)
    Given the following users exist
      |id|email         |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    And an event with the name "testEvent" exists
    And all but the user with id 10 are part of the event
    When I send a POST request to "/api/event/students/list" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "event" : {
          "id": "1"
        }
      }
      """
    And the JSON response should be:
    """
      {"errors": ["Insufficient privileges"]}
     """
    Then the response status should be "403"

  Scenario: Unsuccessfully Get users for an event (no event connected to id)
    Given the following users exist
      |id|email         |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    And an event with the name "testEvent" exists
    And all but the user with id 10 are part of the event
    When I send a POST request to "/api/event/students/list" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        },
        "event" : {
          "id": "2"
        }
      }
      """
    And the JSON response should be:
    """
      {"errors": ["Couldn't find Event with id=2"]}
     """
    Then the response status should be "422"