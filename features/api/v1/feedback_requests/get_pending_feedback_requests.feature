Feature: Pending Feedback Requests

  Background:
    Given I send and accept JSON

  Scenario: get pending feedback requests
  Given the following users exist
    |id|email           |first_name |last_name  |password |authentication_token|role |
    |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
    |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
    |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
    |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
    |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
  Given the following feedback_requests exist
    |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
    |10|1 |1      |10       |11 | false |
    |11|1 |1     |10       |12 | false |
    |12|1 |1      |10       |13 | false |
    |13|1 |1     |10       |14 | false |
    |14|1 |1      |11       |10 | false |
  When I send a POST request to "/api/feedback/pending" with the following:
  """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        }
      }
      """
  Then the response status should be "200"
  And the JSON response should be:
  """
      {
        "feedback_requests": [
            {
              "category_id": 1,
              "event_id": 1,
              "reply": false,
              "solicited_id": 10,
              "solicitor_id": 11
            }
        ]
      }
    """

  Scenario: get pending feedback request (none available)
    Given the following users exist
      |id|email           |first_name |last_name  |password |authentication_token|role |
      |10|user1@cmu.edu |First      |User       |test1234 |auth_token_123      |user |
      |11|user2@cmu.edu |Second     |User       |test1234 |auth_token_223      |user |
      |12|user3@cmu.edu |Third      |User       |test1234 |auth_token_323      |user |
      |13|user4@cmu.edu |Fourth     |User       |test1234 |auth_token_423      |user |
      |14|user5@cmu.edu |Fifth      |User       |test1234 |auth_token_523      |admin|
    Given the following feedback_requests exist
      |id|event_id           |category_id |solicitor_id  |solicited_id | reply |
      |10|1 |1      |10       |11 | false |
      |11|1 |1     |10       |12 | false |
      |12|1 |1      |10       |13 | false |
      |13|1 |1     |10       |14 | false |
      |14|1 |1      |11       |10 | true |
    When I send a POST request to "/api/feedback/pending" with the following:
    """
      {
        "user" : {
          "email": "user1@cmu.edu",
          "auth_token": "auth_token_123"
        }
      }
      """
    Then the response status should be "200"
    And the JSON response should be:
    """
      {
        "feedback_requests": [

        ]
      }
    """