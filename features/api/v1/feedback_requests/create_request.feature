Feature: Feedback Request

  Background:
    Given I send and accept JSON

  Scenario: create Direct Feedback Request Successfully
    Given "Adam" is a user with email id "user@cmu.edu" and password "password123"
    And "Adam" authentication token is "auth_token"
    When I send a POST request to "/api/feedback/request" with the following:
    """
      {
        "user" : {
          "email": "user@cmu.edu",
          "auth_token": "auth_token"
        },
        "feedback_request" : {
          "event_id": "1",
          "categories_ids": "1,3",
          "solicited_ids": "1,2,3,4,5"
        }
      }
      """
    Then the response status should be "201"
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|1|
      |category_id|1|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|1|
      |category_id|3|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|2|
      |category_id|1|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|2|
      |category_id|3|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|3|
      |category_id|1|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|3|
      |category_id|3|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|4|
      |category_id|1|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|4|
      |category_id|3|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|5|
      |category_id|1|
      |reply|false|
    And a feedback request for the user with email "user@cmu.edu" should be present with the following
      |event_id|1|
      |solicited_id|5|
      |category_id|3|
      |reply|false|

  Scenario: create Direct Feedback Request UnSuccessfully
    Given "Adam" is a user with email id "user@cmu.edu" and password "password123"
    And "Adam" authentication token is "auth_token"
    When I send a POST request to "/api/feedback/request" with the following:
    """
      {
        "user" : {
          "email": "user@cmu.edu",
          "auth_token": "not_auth_token"
        },
        "feedback_request" : {
          "event_id": "1",
          "categories_ids": "1,3",
          "solicited_ids": "1,2,3,4,5"
        }
      }
      """
    Then the response status should be "401"
    And the JSON response should be:
    """
      {"errors" : ["Invalid login"]}
    """