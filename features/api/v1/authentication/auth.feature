Feature: Authentication

  Background:
    Given I send and accept JSON

  Scenario: Successful log in
    Given "Adam Smith" is a user with role "user", email id "user@cmu.edu" and password "password123"
    When I send a POST request to "/api/users/sign_in" with the following:
    """
      {
        "user" : {
          "email": "user@cmu.edu",
          "password": "password123"
        }
      }
      """
    Then the response status should be "200"
    And the JSON response should have "auth_token"
    And the JSON response at "auth_token" should be a string
    And the JSON response at "first_name" should be a string
    And the JSON response at "last_name" should be a string
    And the JSON response at "email" should be "user@cmu.edu"
    And the JSON response at "first_name" should be "Adam"
    And the JSON response at "last_name" should be "Smith"
    Given I keep the JSON response at "auth_token" as "AUTH_TOKEN"
    Then the user with email "user@cmu.edu" should have "%{AUTH_TOKEN}" as his authentication_token