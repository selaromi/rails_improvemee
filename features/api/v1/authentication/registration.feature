Feature: Sign Up

    Background:
      Given I send and accept JSON

    Scenario: Successful sign up
      When I send a POST request to "/api/users" with the following:
      """
      {
        "user" : {
          "first_name": "Saul",
          "last_name": "Hudson",
          "email": "slash@cmu.edu",
          "password": "sekr@t123",
          "password_confirmation": "sekr@t123",
          "role" : "user"
        },
        "user_setting" : {
          "program_name": "Saul",
          "nickname": "Saully",
          "graduation_year": "2004"
        }
      }
      """
      Then the response status should be "201"
      And the JSON response should have "auth_token"
      And the JSON response at "auth_token" should be a string
      And the JSON response at "role" should be "user"
      And the JSON response at "first_name" should be "Saul"
      And the JSON response at "last_name" should be "Hudson"
      Given I keep the JSON response at "auth_token" as "AUTH_TOKEN"
      Then the user with email "slash@cmu.edu" should have "%{AUTH_TOKEN}" as his authentication_token
      And a "user" should be present with the following
        |first_name|Saul|
        |last_name|Hudson|
        |email|slash@cmu.edu|
      And a "user_settings" should be present for the user with email "slash@cmu.edu" with the following
        |program_name|Saul|
        |nickname|Saully|
        |graduation_year|2004|
        |overall_position|0|

  Scenario: Successful sign up w/out user settings
    When I send a POST request to "/api/users" with the following:
    """
      {
        "user" : {
          "first_name": "Saul",
          "last_name": "Hudson",
          "email": "slash@cmu.edu",
          "password": "sekr@t123",
          "password_confirmation": "sekr@t123"
        },
        "user_setting" : {
          "program_name": "",
          "nickname": "",
          "graduation_year": ""
        }
      }
      """
    Then the response status should be "201"
    And the JSON response should have "auth_token"
    And the JSON response at "auth_token" should be a string
    And the JSON response at "role" should be "user"
    And the JSON response at "first_name" should be "Saul"
    And the JSON response at "last_name" should be "Hudson"
    Given I keep the JSON response at "auth_token" as "AUTH_TOKEN"
    Then the user with email "slash@cmu.edu" should have "%{AUTH_TOKEN}" as his authentication_token
    And a "user" should be present with the following
      |first_name|Saul|
      |last_name|Hudson|
      |email|slash@cmu.edu|
    And a "user_settings" should be present for the user with email "slash@cmu.edu" with the following
      |overall_position|0|


    Scenario: Passwords do not match
      When I send a POST request to "/api/users" with the following:
      """
      {
        "user" : {
          "first_name": "Kobe",
          "last_name": "Bryant",
          "email": "kobe@cmu.edu",
          "password": "kobe1234",
          "password_confirmation": "kobe12345"
        }
      }
      """
      Then the response status should be "422"
      And the JSON response should be:
      """
      {"errors" : ["Password confirmation doesn't match Password"]}
      """

    Scenario: Email is already taken
      Given "Adam Smith" is a user with role "user", email id "user@cmu.edu" and password "password123"
      When I send a POST request to "/api/users" with the following:
      """
      {
        "user" : {
          "first_name": "Kobe",
          "last_name": "Bryant",
          "email": "user@cmu.edu",
          "password": "kobe1234",
          "password_confirmation": "kobe1234"
        }
      }
      """
      Then the response status should be "422"
      And the JSON response should be:
      """
      {"errors" : ["Email has already been taken"]}
      """

    Scenario: Email not from CMU
    When I send a POST request to "/api/users" with the following:
    """
      {
        "user" : {
          "first_name": "Kobe",
          "last_name": "Bryant",
          "email": "user@gmail.com",
          "password": "kobe1234",
          "password_confirmation": "kobe1234"
        }
      }
      """
    Then the response status should be "422"
    And the JSON response should be:
    """
      {"errors" : ["Only CMU students for now"]}
      """