class CreateUserQuestions < ActiveRecord::Migration
  def change
    create_table :user_questions do |t|
      t.integer :user_id
      t.integer :question_id
      t.float :score

      t.timestamps
    end

    add_index :user_questions, :user_id
  end
end
