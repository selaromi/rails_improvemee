class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.string :name, :null => false
      t.string :description, :null => false
      t.datetime :start_date
      t.datetime :end_date
      t.string :entity_type
      t.integer :owner
      t.string :preferred_categories

      t.timestamps
    end
  end
end
