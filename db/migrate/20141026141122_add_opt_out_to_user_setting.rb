class AddOptOutToUserSetting < ActiveRecord::Migration
  def change
    add_column :user_settings, :optOut, :boolean, :null => true
  end
end
