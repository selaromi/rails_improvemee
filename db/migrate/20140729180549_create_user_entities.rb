class CreateUserEntities < ActiveRecord::Migration
  def change
    create_table :user_entities do |t|
      t.integer :user_id
      t.integer :entity_id
      t.boolean :admin, default: false

      t.timestamps
    end
    add_index :user_entities, [:user_id,:entity_id],                :unique => true
  end
end
