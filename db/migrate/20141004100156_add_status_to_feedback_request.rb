class AddStatusToFeedbackRequest < ActiveRecord::Migration
  def change
    add_column :feedback_requests, :status, :integer, :null => false, :default => 0
  end
end
