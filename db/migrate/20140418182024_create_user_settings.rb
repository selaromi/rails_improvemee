class CreateUserSettings < ActiveRecord::Migration
  def change
    create_table :user_settings do |t|
      t.string :program_name
      t.integer :overall_position, :null => false, default: 0
      t.string :nickname
      t.integer :graduation_year
      t.integer :user_id

      t.timestamps
    end
    add_index :user_settings, :user_id
  end
end
