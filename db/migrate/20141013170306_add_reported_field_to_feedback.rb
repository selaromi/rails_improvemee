class AddReportedFieldToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :reported, :boolean, :null => false, :default => false
  end
end
