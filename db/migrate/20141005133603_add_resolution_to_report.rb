class AddResolutionToReport < ActiveRecord::Migration
  def change
    add_column :reports, :approved, :boolean, null: true
  end
end
