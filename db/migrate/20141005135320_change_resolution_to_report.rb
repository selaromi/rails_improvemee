class ChangeResolutionToReport < ActiveRecord::Migration
  def change
    remove_column :reports, :approved, :boolean
    add_column :reports, :approved, :boolean, null: true
  end
end
