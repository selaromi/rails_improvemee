class AddCategoriesPercentiles < ActiveRecord::Migration
  def change
    create_table :user_category_percentile do |t|
        t.integer :user_id
        t.integer :category_id
        t.float :percentile
    end
  end
end
