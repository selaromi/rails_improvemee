class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :category_id, :null => false
      t.string :text, :null => false
      t.integer :overall_score, :null => false, default: 0

      t.timestamps
    end

    add_index :questions, :category_id
  end
end
