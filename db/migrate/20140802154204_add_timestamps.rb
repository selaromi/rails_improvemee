class AddTimestamps < ActiveRecord::Migration
  def change
    add_column(:user_question_percentiles, :created_at, :datetime)
    add_column(:user_question_percentiles, :updated_at, :datetime)
    add_column(:user_category_percentiles, :created_at, :datetime)
    add_column(:user_category_percentiles, :updated_at, :datetime)
  end
end
