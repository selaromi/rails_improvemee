class AddOverallPercentiles2 < ActiveRecord::Migration
  def change
    add_column :user_questions, :percentile, :float
  end
end
