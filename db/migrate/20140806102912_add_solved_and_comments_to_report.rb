class AddSolvedAndCommentsToReport < ActiveRecord::Migration
  def change
    add_column :reports, :solved, :boolean, default: false
    add_column :reports, :comments, :string
  end
end
