class CreatePendingFeedbackReports < ActiveRecord::Migration
  def change
    create_table :pending_feedback_reports do |t|
      t.integer :feedback_id

      t.timestamps
    end
  end
end
