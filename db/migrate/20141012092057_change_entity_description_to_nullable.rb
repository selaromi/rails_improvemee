class ChangeEntityDescriptionToNullable < ActiveRecord::Migration
  def change
    change_column :entities, :description, :string, :null => true
  end
end
