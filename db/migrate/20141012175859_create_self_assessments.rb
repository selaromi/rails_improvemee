class CreateSelfAssessments < ActiveRecord::Migration
  def change
    create_table :self_assessments do |t|
      t.integer :user_id,:null => false
      t.integer :category_id,:null => false
      t.string :score, :null => false

      t.timestamps
    end
  end
end
