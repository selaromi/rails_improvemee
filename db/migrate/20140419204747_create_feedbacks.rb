class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.integer :feedback_request_id
      t.integer :sender_id,:null => false
      t.integer :receiver_id,:null => false
      t.integer :event_id
      t.boolean :is_anonymous, default: false
      t.text    :comments
      t.boolean :seen,:null => false, default: false
      t.timestamps
    end

    add_index :feedbacks, :feedback_request_id
    add_index :feedbacks, :sender_id
    add_index :feedbacks, :receiver_id
    add_index :feedbacks, :event_id

  end
end
