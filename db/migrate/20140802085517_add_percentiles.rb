class AddPercentiles < ActiveRecord::Migration
  def add_feedback_question_table
    create_table :user_question_percentile do |t|
        t.integer :user_id
        t.integer :question_id
        t.float :percentile
    end
  end

  def add_feedback_category_table
      create_table :user_category_percentile do |t|
          t.integer :user_id
          t.integer :category_id
          t.float :percentile
      end
  end

  def change_user
      change_table :user_settings do |t|
          t.float :percentile
      end
  end
end
