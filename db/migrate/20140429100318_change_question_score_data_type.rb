class ChangeQuestionScoreDataType < ActiveRecord::Migration
  def change
    remove_column :questions, :overall_score, :integer
    add_column :questions, :overall_score, :float
  end
end
