class AddOwnerToEvent < ActiveRecord::Migration
  def change
    add_column :events, :owner, :integer

    add_index :events, :owner
  end
end
