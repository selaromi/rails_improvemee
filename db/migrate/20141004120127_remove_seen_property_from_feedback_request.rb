class RemoveSeenPropertyFromFeedbackRequest < ActiveRecord::Migration
  def change
    remove_column :feedback_requests, :reply
  end
end
