class CreateUserCustomgroupUsers < ActiveRecord::Migration
  def change
    create_table :user_customgroup_users do |t|
      t.integer :user_customgroup_id
      t.integer :user_id

      t.timestamps
    end
  end
end
