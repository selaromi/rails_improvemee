class AddFeedbackIdToUserQuestion < ActiveRecord::Migration
  def change
    add_column :user_questions, :feedback_id, :integer
  end
end
