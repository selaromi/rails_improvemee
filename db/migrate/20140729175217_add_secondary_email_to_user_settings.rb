class AddSecondaryEmailToUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :secondary_email, :string
  end
end
