class AddPreferredCategoriesToEvent < ActiveRecord::Migration
  def change
    add_column :events, :preferred_categories, :string
  end
end
