class AddCategoryIdToFeedbackRequest < ActiveRecord::Migration
  def change
    add_column :feedback_requests, :category_id, :integer
    add_index :feedback_requests, :category_id
  end

end
