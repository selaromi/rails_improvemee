class AddLicenceAgreementToUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :licence_agreement, :boolean, default: false
  end
end