class AddOverallPercentiles < ActiveRecord::Migration
  def change
    def change_user
        change_table :user_settings do |t|
            t.float :percentile
        end
    end
  end
end
