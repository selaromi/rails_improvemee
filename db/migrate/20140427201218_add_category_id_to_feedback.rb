class AddCategoryIdToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :category_id, :integer
    add_index :feedbacks, :category_id
  end
end
