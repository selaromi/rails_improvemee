class ChageOverallScoreDatatype < ActiveRecord::Migration
  def change
    remove_column :categories, :overall_score, :integer
    add_column :categories, :overall_score, :float
  end
end
