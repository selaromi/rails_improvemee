class AddFeedbackIdToUserCategory < ActiveRecord::Migration
  def change
    add_column :user_categories, :feedback_id, :integer
  end
end
