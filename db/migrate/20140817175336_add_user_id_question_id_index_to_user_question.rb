class AddUserIdQuestionIdIndexToUserQuestion < ActiveRecord::Migration
  def change
    add_index :user_questions, [:user_id, :question_id]
  end
end
