class CreateUserCommunications < ActiveRecord::Migration
  def change
    create_table :user_communications do |t|
      t.integer :user_id, :null => true
      t.integer :communication_id
      t.integer :frequency, :null => true

      t.timestamps
    end
  end
end
