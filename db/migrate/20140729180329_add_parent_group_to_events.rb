class AddParentGroupToEvents < ActiveRecord::Migration
  def change
    add_column :events, :parent_entity, :integer
  end
end
