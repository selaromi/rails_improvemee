class AddPriorityToFeedBackRequests < ActiveRecord::Migration
  def change
    add_column :feedback_requests, :high_priority, :boolean, default: false
  end
end
