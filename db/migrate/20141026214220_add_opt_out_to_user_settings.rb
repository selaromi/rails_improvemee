class AddOptOutToUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :opt_out, :boolean, default: false
  end
end
