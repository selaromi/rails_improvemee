class AddOverallPercentiles3 < ActiveRecord::Migration
  def change
      remove_column :user_questions, :percentile
      add_column :user_settings, :percentile, :float
  end
end
