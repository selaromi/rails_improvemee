class CreateFeedbackQuestionScores < ActiveRecord::Migration
  def change
    create_table :feedback_question_scores do |t|
      t.integer :feedback_id,:null => false
      t.integer :question_id,:null => false
      t.integer :score,:null => false

      t.timestamps
    end

    add_index :feedback_question_scores, :feedback_id
    add_index :feedback_question_scores, :question_id
  end
end
