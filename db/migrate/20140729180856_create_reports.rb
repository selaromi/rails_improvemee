class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :feedback_id
      t.datetime :action_taken_at

      t.timestamps
    end
  end
end
