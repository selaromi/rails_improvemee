class CreateAppSettings < ActiveRecord::Migration
  def change
    create_table :app_settings do |t|
      t.boolean :sends_email

      t.timestamps
    end
  end
end
