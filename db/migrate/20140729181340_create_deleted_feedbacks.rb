class CreateDeletedFeedbacks < ActiveRecord::Migration
  def change
    create_table :deleted_feedbacks do |t|
      t.hstore :data

      t.timestamps
    end
  end
end
