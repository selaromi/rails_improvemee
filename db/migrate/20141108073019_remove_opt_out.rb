class RemoveOptOut < ActiveRecord::Migration
  def change
    remove_column :user_settings, :optOut
  end
end
