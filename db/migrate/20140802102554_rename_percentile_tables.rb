class RenamePercentileTables < ActiveRecord::Migration
  def change
    rename_table :user_category_percentile, :user_category_percentiles
    rename_table :user_question_percentile, :user_question_percentiles
  end
end
