class AddQuestionsPercentiles < ActiveRecord::Migration
  def change
    create_table :user_question_percentile do |t|
      t.integer :user_id
      t.integer :question_id
      t.float :percentile
     end
  end
end
