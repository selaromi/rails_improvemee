class CreateFeedbackRequests < ActiveRecord::Migration
  def change
    create_table :feedback_requests do |t|
      t.integer :solicitor_id,:null => false
      t.integer :solicited_id,:null => false
      t.integer :event_id
      t.boolean :reply,:null => false, default:false

      t.timestamps
    end


    add_index :feedback_requests, :solicited_id
    add_index :feedback_requests, :solicitor_id
    add_index :feedback_requests, :event_id

    #add_index 'feedback_requests', ["solicitor_id", "solicited_id", "event_id"], name: "solicitor_solicited_event", unique: true, using: :btree
  end
end
