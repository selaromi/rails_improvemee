class AddCreatorTypeForEvent < ActiveRecord::Migration
  def change
    add_column :events, :creator_role, :string
  end
end
