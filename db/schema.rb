# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141108221437) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "app_settings", force: true do |t|
    t.boolean  "sends_email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name",          null: false
    t.string   "description",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "overall_score"
  end

  create_table "deleted_feedbacks", force: true do |t|
    t.hstore   "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "entities", force: true do |t|
    t.string   "name",                 null: false
    t.string   "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "entity_type"
    t.integer  "owner"
    t.string   "preferred_categories"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "entity_categories", force: true do |t|
    t.integer  "entity_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_categories", force: true do |t|
    t.integer  "event_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "name",                 null: false
    t.string   "description"
    t.datetime "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owner"
    t.string   "preferred_categories"
    t.integer  "parent_entity"
    t.string   "creator_role"
    t.date     "start_date"
  end

  add_index "events", ["owner"], name: "index_events_on_owner", using: :btree

  create_table "feedback_question_scores", force: true do |t|
    t.integer  "feedback_id", null: false
    t.integer  "question_id", null: false
    t.integer  "score",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "feedback_question_scores", ["feedback_id"], name: "index_feedback_question_scores_on_feedback_id", using: :btree
  add_index "feedback_question_scores", ["question_id"], name: "index_feedback_question_scores_on_question_id", using: :btree

  create_table "feedback_requests", force: true do |t|
    t.integer  "solicitor_id",                  null: false
    t.integer  "solicited_id",                  null: false
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.boolean  "high_priority", default: false
    t.integer  "status",        default: 0,     null: false
  end

  add_index "feedback_requests", ["category_id"], name: "index_feedback_requests_on_category_id", using: :btree
  add_index "feedback_requests", ["event_id"], name: "index_feedback_requests_on_event_id", using: :btree
  add_index "feedback_requests", ["solicited_id"], name: "index_feedback_requests_on_solicited_id", using: :btree
  add_index "feedback_requests", ["solicitor_id"], name: "index_feedback_requests_on_solicitor_id", using: :btree

  create_table "feedbacks", force: true do |t|
    t.integer  "feedback_request_id"
    t.integer  "sender_id",                           null: false
    t.integer  "receiver_id",                         null: false
    t.integer  "event_id"
    t.boolean  "is_anonymous",        default: false
    t.text     "comments"
    t.boolean  "seen",                default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.boolean  "reported",            default: false, null: false
  end

  add_index "feedbacks", ["category_id"], name: "index_feedbacks_on_category_id", using: :btree
  add_index "feedbacks", ["event_id"], name: "index_feedbacks_on_event_id", using: :btree
  add_index "feedbacks", ["feedback_request_id"], name: "index_feedbacks_on_feedback_request_id", using: :btree
  add_index "feedbacks", ["receiver_id"], name: "index_feedbacks_on_receiver_id", using: :btree
  add_index "feedbacks", ["sender_id"], name: "index_feedbacks_on_sender_id", using: :btree

  create_table "pending_feedback_reports", force: true do |t|
    t.integer  "feedback_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "questions", force: true do |t|
    t.integer  "category_id",   null: false
    t.string   "text",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "overall_score"
  end

  add_index "questions", ["category_id"], name: "index_questions_on_category_id", using: :btree

  create_table "reports", force: true do |t|
    t.integer  "feedback_id"
    t.datetime "action_taken_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "solved",          default: false
    t.string   "comments"
    t.boolean  "approved"
  end

  create_table "self_assessments", force: true do |t|
    t.integer  "user_id",     null: false
    t.integer  "category_id", null: false
    t.string   "score",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_categories", force: true do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.float    "overall_score"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "feedback_id"
  end

  add_index "user_categories", ["category_id"], name: "index_user_categories_on_category_id", using: :btree
  add_index "user_categories", ["user_id"], name: "index_user_categories_on_user_id", using: :btree

  create_table "user_category_percentiles", force: true do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.float    "percentile"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_communications", force: true do |t|
    t.integer  "user_id"
    t.integer  "communication_id"
    t.integer  "frequency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_customgroup_users", force: true do |t|
    t.integer  "user_customgroup_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_entities", force: true do |t|
    t.integer  "user_id"
    t.integer  "entity_id"
    t.boolean  "admin",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_entities", ["user_id", "entity_id"], name: "index_user_entities_on_user_id_and_entity_id", unique: true, using: :btree

  create_table "user_events", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "event_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_events", ["event_id"], name: "index_user_events_on_event_id", using: :btree
  add_index "user_events", ["user_id"], name: "index_user_events_on_user_id", using: :btree

  create_table "user_question_percentiles", force: true do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.float    "percentile"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_questions", force: true do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.float    "score"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "feedback_id"
  end

  add_index "user_questions", ["user_id", "question_id"], name: "index_user_questions_on_user_id_and_question_id", using: :btree
  add_index "user_questions", ["user_id"], name: "index_user_questions_on_user_id", using: :btree

  create_table "user_settings", force: true do |t|
    t.string   "program_name"
    t.integer  "overall_position",  default: 0,     null: false
    t.string   "nickname"
    t.integer  "graduation_year"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "secondary_email"
    t.float    "percentile"
    t.boolean  "licence_agreement", default: false
    t.boolean  "opt_out",           default: false
  end

  add_index "user_settings", ["user_id"], name: "index_user_settings_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "role",                                null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
