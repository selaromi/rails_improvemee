ImproveMee BackEnd
===========

Uses rails-api.

To run the tests do: bundle exec rake
The test files are all in the features folder.

Based on the rails-4-api template by emilsonman. (http://www.emilsoman.com/blog/2013/05/18/building-a-tested/)After I published my blog post on Building a Tested, Documented and Versioned JSON API Using Rails 4, many readers asked me for