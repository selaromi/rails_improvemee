class AsyncMailer < ActionMailer::Base
  include Sidekiq::Worker # can be used if we want to use a worker
  default from: 'ImproveMee - Notifications  <notifications@improvemee.com>'

  EVENT_WELCOME_EMAIL_TYPE = 1
  SEND_FEEDBACK_REQUEST_REMINDER_TYPE = 2
  SEND_FEEDBACK_RECEIVED_REPORT_TYPE = 3

  def perform(mail_type)
    @mail_type = mail_type
    case @mail_type
      #when EVENT_WELCOME_EMAIL_TYPE
      #  send_event_email_welcome
      when SEND_FEEDBACK_REQUEST_REMINDER_TYPE
        send_feedback_request_reminder
      when SEND_FEEDBACK_RECEIVED_REPORT_TYPE
        send_feedback_received_report
      else

    end
  end

  def test email
    mail(to:email, subject: 'Welcome')
  end
  
  #def send_event_email_welcome(user_list)
  #  user_list.each { |user|
  #    @user = user
  #    @url = url
  #    email = user.user_setting.secondary_email || user.email
  #    mail(from: %q{info@improvemee.com}, to:email, subject: 'Welcome')
  #  }
  #end

  def send_feedback_received_report
    user_list = User.all
    @default_frequency = UserCommunication.where(user_id:nil,communication_id: SEND_FEEDBACK_RECEIVED_REPORT_TYPE).first.frequency
    user_list.each { |user|
      @user = user
      @user_frequency = UserCommunication.where(user_id:@user.id,communication_id: SEND_FEEDBACK_RECEIVED_REPORT_TYPE).first
      if @user_frequency == nil
        frequency = @default_frequency
      else
        if @user_frequency.frequency == -1
          next
        elsif @user_frequency.frequency != @default_frequency
          frequency = @user_frequency.frequency
        else
          frequency = @default_frequency

        end
      end
      if(frequency == 1 || (frequency == 7 && Time.now.wday == 1))
        @new_feedback_event =  {}
        Feedback.where(receiver:@user,seen:false).where('created_at > ?',(DateTime.now-frequency).beginning_of_day()).map { |x|
          if @new_feedback_event[x.sender.first_name] == nil then
            @new_feedback_event[x.sender.first_name] = [x.category.name]
          else
            @new_feedback_event[x.sender.first_name].push(x.category.name)
          end
        }
        next if @new_feedback_event.empty?
        @total_new_feedback = @new_feedback_event.length
        mail(to:user.email, subject: 'You have received feedback! ').deliver
      end
    }
  end

  def send_feedback_request_reminder
    user_list = User.all
    @default_frequency = UserCommunication.where(user_id:nil,communication_id: SEND_FEEDBACK_REQUEST_REMINDER_TYPE).first.frequency
    user_list.each { |user|
      @user = user
      @user_frequency = UserCommunication.where(user_id:@user.id,communication_id: SEND_FEEDBACK_REQUEST_REMINDER_TYPE).first
      if @user_frequency == nil
        frequency = @default_frequency
      else
        if @user_frequency.frequency == -1
          next
        elsif @user_frequency.frequency != @default_frequency
          frequency = @user_frequency.frequency
        else
          frequency = @default_frequency
        end
      end
      if(frequency == 1 || (frequency == 7 && Time.now.wday == 1))
        @new_feedback_request_event = {}
        FeedbackRequest.where(solicited:@user,status:0).where('created_at > ?',(DateTime.now-frequency).beginning_of_day()).map { |x|
          if @new_feedback_request_event[x.solicitor.first_name] == nil then
            @new_feedback_request_event[x.solicitor.first_name] = [[x.event.name,x.category.name]]
          else  @new_feedback_request_event[x.solicitor.first_name].push([x.event.name,x.category.name])
          end
        }
        next if @new_feedback_request_event.empty?
        mail(to:user.email, subject: 'Your classmates are asking for your feedback!').deliver
      end
    }
  end


end

