class UserMailer < ActionMailer::Base
  default from: 'ImproveMee - Notifications  <notifications@improvemee.com>'

  def welcome_email(user)
    @user = user
    @url  = 'http://secret-hamlet-1764.herokuapp.com/'
    mail(to: @user.email, subject: 'Welcome to Basecamp') if AppSetting.can_send_mail?
  end

  def password_reset_email(user, url)
    @user = user
    @url = url
    email = user.email || user.user_setting.secondary_email
    puts user.inspect
    puts email
    mail(from: 'contact@improvemee.com', to: email, subject: 'Password reset request')
  end
end
