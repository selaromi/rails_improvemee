object @user_setting

attributes :nickname, :program_name, :secondary_email, :graduation_year, :licence_agreement, :opt_out
