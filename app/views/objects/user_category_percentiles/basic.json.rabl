object @user_category_percentiles

attributes :id, :percentile, :created_at, :updated_at

child :category do
	extends 'objects/categories/basic'
end