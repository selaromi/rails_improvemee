object @user_questions

attributes :id, :score, :created_at, :updated_at

child :question do
	extends 'objects/questions/basic'
end