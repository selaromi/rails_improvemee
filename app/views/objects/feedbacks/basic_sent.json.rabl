object @feedback

attributes :id, :is_anonymous, :comments, :seen, :created_at, :updated_at

child :receiver => :receiver do
	extends 'objects/users/basic'
end

child :event do
	extends 'objects/events/basic'
end

child :category do
	extends 'objects/categories/basic'
end

child :feedback_question_scores, :object_root => false do
		extends 'objects/feedback_question_scores/basic'
end

node :reported, if: lambda {|x| x.sender == @user } do |feedback| Report.where(feedback_id: feedback.id).any? end