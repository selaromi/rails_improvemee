object @user_question_percentiles

attributes :id, :percentile, :created_at, :updated_at

child :question do
	extends 'objects/questions/basic'
end