object @user

attributes :first_name, :last_name, :role, :email
attribute :authentication_token => :auth_token
child :user_setting do
    extends 'objects/user_settings/basic'
end