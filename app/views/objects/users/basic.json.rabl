object @user

attributes :id, :first_name, :last_name, :role
child :user_setting, :object_root => false do
    attribute :opt_out
end