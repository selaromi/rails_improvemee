object @user_categories

attributes :id, :position, :overall_score, :created_at, :updated_at

child :category do
	extends 'objects/categories/extended'
end
