object @event

attributes :id, :name, :description, :start_date, :end_date, :creator_role

child :parent_entity => :parent_entity do
	extends 'objects/entities/basic'
end

child :owner => :owner do
	extends 'objects/users/basic'
end

child :users, :object_root => false do
	extends 'objects/users/basic'
end

child :categories, :object_root => false do
    attribute :id, :name
end