object @event

attributes :id, :name, :description, :start_date, :end_date, :creator_role

child :parent_entity do
	extends 'objects/entities/basic'
end