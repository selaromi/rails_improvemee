object @entity

attributes :id, :name, :description, :start_date, :end_date, :entity_type

child :users, :object_root => false do
	attributes :id, :first_name, :last_name
end

child :events, :object_root => false do
	attributes :id, :name, :description, :start_date, :end_date
end

child :owner, :object_root => false do
    attribute :id, :first_name, :last_name
end

child :categories, :object_root => false do
    attribute :id, :name
end