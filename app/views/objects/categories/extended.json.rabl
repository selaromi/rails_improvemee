object @category

attributes :id, :name, :description

node(:feedback_total) { |category| category.feedbacks.where(receiver_id:@user.id).count}

child :questions, :object_root => false do
	attributes :id, :text
	node do |question|
		#{ :score => UserQuestion.find_by(question_id:question).score }
		{ :score => UserQuestion.where(user_id:@user.id, question_id:question).last.score }
	end
end