object @feedback_request

attributes :id, :status, :high_priority, :created_at

child :solicitor => :solicitor do
	extends 'objects/users/basic'
end

child :solicited => :solicited do
	extends 'objects/users/basic'
end

child :event do
	extends 'objects/events/basic'
end

child :category do
	extends 'objects/categories/basic'
end