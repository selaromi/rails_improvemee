object @user_customgroup

attributes :id, :name, :description, :created_at, :updated_at

child :owner do
	extends 'objects/users/basic'
end

child :users do
	extends 'objects/users/basic'
end

