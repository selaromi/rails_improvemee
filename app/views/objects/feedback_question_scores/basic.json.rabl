object @feedback_question_score

parent = root_object.dup

child :question, :object_root => false do
	extends 'objects/questions/basic'
	node(:score) { parent.score }
end

