class UserStatistics
  require 'benchmark'
  #TODO: Sidekiq
  include Sidekiq::Worker # can be used if we want to use a worker
  #
  BASECAMP_EVENT_ID = 1

  def perform(category_id, feedback_id, receiver_id, answers)
    #new(category_id, feedback_id, receiver_id, answers)
    @category_id = category_id
    @receiver_id = receiver_id
    @feedback_id = feedback_id
    @answers = answers
    @questions_combined_score = 0
    update
  end

  #def initialize (category_id, feedback_id, receiver_id, answers)
  #  @category_id = category_id
  #  @receiver_id = receiver_id
  #  @feedback_id = feedback_id
  #  @answers = answers
  #  @questions_combined_score = 0
  #  @user_category_score = Hash.new()
  #end

  def update
    #feedback_question_scores
    update_user_questions
    update_user_category
    UserPercentile.perform_async
  end

  def self.update_question_percentile_batch_job
    Question.all.each do |question|
      if question.category.id == BASECAMP_EVENT_ID
        next
      end
      user_scores = Hash.new
      question.feedback_question_scores.all.each do |u|
        if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
          next
        end
        if user_scores[u.feedback.receiver.id].nil?
          user_scores[u.feedback.receiver.id] = u.score
        else
          user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
        end
      end

      ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

      number_of_users = ordered_user_scores.length

      percentiles = Hash.new
      i = 1.0
      ordered_user_scores.each_pair do |k,v|
        percentile = (i / number_of_users) * 100
        aux = UserQuestionPercentile.where(user_id: k, question_id: question.id).first
        if aux.nil?
          UserQuestionPercentile.create(user_id: k, question_id: question.id, percentile: percentile)
        else
          aux.update!(percentile: percentile)
        end
        i = i + 1
      end
    end
  end

  def self.update_category_percentile_batch_job
    result = Hash.new
    Category.all.each do |category|
      if category.id == BASECAMP_EVENT_ID
        next
      end
      user_scores = Hash.new
      Question.where(category_id: category.id).each do |question|
        question.feedback_question_scores.all.each do |u|
          if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
            next
          end
          #puts u.feedback.receiver.id
          if user_scores[u.feedback.receiver.id].nil?
            user_scores[u.feedback.receiver.id] = u.score
          else
            user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
          end
        end
      end

      ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

      number_of_users = ordered_user_scores.length

      percentiles = Hash.new
      i = 1.0
      ordered_user_scores.each_pair do |k,v|
        percentile = (i / number_of_users) * 100
        aux = UserCategoryPercentile.where(user_id: k, category_id: category.id).first
        if aux.nil?
          UserCategoryPercentile.create(user_id: k, category_id: category.id, percentile: percentile)
        else
          aux.update!(percentile: percentile)
        end
        i = i + 1
      end
    end
  end

  def self.update_overall_percentile_batch_job
    result = Hash.new
    user_scores = Hash.new
    Question.all.each do |question|
      if question.category.id == BASECAMP_EVENT_ID
        next
      end
      question.feedback_question_scores.all.each do |u|
        if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
          next
        end
        #puts u.feedback.receiver.id
        if user_scores[u.feedback.receiver.id].nil?
          user_scores[u.feedback.receiver.id] = u.score
        else
          user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
        end
      end
    end

    ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

    number_of_users = ordered_user_scores.length

    percentiles = Hash.new
    i = 1.0
    ordered_user_scores.each_pair do |k,v|
      percentile = (i / number_of_users) * 100
      aux = UserSetting.where(user_id: k).first
      if aux.nil?
        #nothing to do here
      else
        aux.update!(percentile: percentile)
      end
      i = i + 1
    end
  end
  
  private
    def feedback_question_scores
      Category.find(@category_id).questions.order(id: :asc).map do |question|
        FeedbackQuestionScore.create(feedback_id:@feedback_id,question_id:question.id,score:@answers.shift)
      end
    end

    def update_user_questions
      feedback_question_scores.each do |qs|
        @existing_questions = UserQuestion.where(user_id: @receiver_id, question_id: qs.question_id ).order(created_at: :desc)
        update_question_combined_score qs.score.to_f
        UserQuestion.create!(user_id: @receiver_id, question_id: qs.question_id, score:new_score(qs.score.to_f).round(2), feedback_id: @feedback_id)
#
#        update_question_overall_score qs
      end
    end

    def update_question_overall_score current_feedback_question_score
      acum_question_score     = 0.0;
      total_question_scores   = 0.0;
      User.all.each do |user|
        last_existing_question = UserQuestion.where(user_id: user.id, question_id: current_feedback_question_score.question_id ).order(created_at: :desc).first
        next unless last_existing_question
        total_question_scores += 1
        acum_question_score   += last_existing_question.score
      end
      question = Question.find(current_feedback_question_score.question_id)
      question.overall_score = acum_question_score / total_question_scores;
      question.save
    end

    def update_question_combined_score last_score
      @questions_combined_score += last_score
    end

    def previous_score
      @existing_questions.first ? @existing_questions.first.score : 0.0
    end

    def new_score last_score
      ((previous_score * @existing_questions.count) + last_score) / (@existing_questions.count + 1)
    end

    def update_user_category
      UserCategory.create!(user_id: @receiver_id, category_id: @category_id, overall_score:next_overall_score.round(2), feedback_id: @feedback_id)
    end

    def existing_categories
      UserCategory.where(user_id: @receiver_id,category_id: @category_id ).order(created_at: :desc)
    end

    def previous_overall_score
      existing_categories.first ? existing_categories.first.overall_score : 0.0
    end

    def next_overall_score
      category_question_count = Category.find(@category_id).questions.count
      (previous_overall_score * existing_categories.count + (@questions_combined_score / category_question_count)) / ( existing_categories.count + 1)
    end

    def update_user_position_in_category
      Category.all.each do |category|
        acum_category_score = 0
        total_category_scores = 0
        user_ranking_since_last_update(category.id).each_with_index do |user_ranking,index|
          acum_category_score += user_ranking.overall_score
          total_category_scores += 1
          uc = UserCategory.find(user_ranking.id)
          if(uc.position.presence && uc.position != index + 1 )
            new_uc = uc.dup
            new_uc.id = nil
            new_uc.position = index+1
            new_uc.save
          else
            uc.update(position: index+1)
          end
          @user_category_score[uc.user_id] = (@user_category_score[uc.user_id]? @user_category_score[uc.user_id] : 0) + uc.overall_score
        end
        category.update(overall_score: acum_category_score/total_category_scores) if (total_category_scores > 0)
      end
    end

    def update_user_position_in_Question

    end

    def user_ranking_since_last_update category_id
      UserCategory.where(category_id: category_id).order(created_at: :desc).to_a
                                                  .uniq {|user_category| user_category.user_id }
                                                  .sort_by {|user_category_sorter| user_category_sorter.overall_score }
                                                  .reverse!
    end

end