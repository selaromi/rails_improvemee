class UserPercentile
  require 'benchmark'
  #TODO: Sidekiq
  include Sidekiq::Worker # can be used if we want to use a worker
  #
  BASECAMP_EVENT_ID = 1

  def perform
    update_question_percentile_batch_job
    update_category_percentile_batch_job
    update_overall_percentile_batch_job
  end

  def update_question_percentile_batch_job
    Question.all.each do |question|
      if question.category.id == BASECAMP_EVENT_ID
        next
      end
      user_scores = Hash.new
      question.feedback_question_scores.all.each do |u|
        if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
          next
        end
        if user_scores[u.feedback.receiver.id].nil?
          user_scores[u.feedback.receiver.id] = u.score
        else
          user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
        end
      end

      ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

      number_of_users = ordered_user_scores.length

      percentiles = Hash.new
      i = 1.0
      ordered_user_scores.each_pair do |k,v|
        percentile = (i / number_of_users) * 100
        aux = UserQuestionPercentile.where(user_id: k, question_id: question.id).first
        if aux.nil?
          UserQuestionPercentile.create(user_id: k, question_id: question.id, percentile: percentile)
        else
          aux.update!(percentile: percentile)
        end
        i = i + 1
      end
    end
  end

  def update_category_percentile_batch_job
    result = Hash.new
    Category.all.each do |category|
      if category.id == BASECAMP_EVENT_ID
        next
      end
      user_scores = Hash.new
      Question.where(category_id: category.id).each do |question|
        question.feedback_question_scores.all.each do |u|
          if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
            next
          end
          #puts u.feedback.receiver.id
          if user_scores[u.feedback.receiver.id].nil?
            user_scores[u.feedback.receiver.id] = u.score
          else
            user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
          end
        end
      end

      ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

      number_of_users = ordered_user_scores.length

      percentiles = Hash.new
      i = 1.0
      ordered_user_scores.each_pair do |k,v|
        percentile = (i / number_of_users) * 100
        aux = UserCategoryPercentile.where(user_id: k, category_id: category.id).first
        if aux.nil?
          UserCategoryPercentile.create(user_id: k, category_id: category.id, percentile: percentile)
        else
          aux.update!(percentile: percentile)
        end
        i = i + 1
      end
    end
  end

  def update_overall_percentile_batch_job
    result = Hash.new
    user_scores = Hash.new
    Question.all.each do |question|
      if question.category.id == BASECAMP_EVENT_ID
        next
      end
      question.feedback_question_scores.all.each do |u|
        if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
          next
        end
        #puts u.feedback.receiver.id
        if user_scores[u.feedback.receiver.id].nil?
          user_scores[u.feedback.receiver.id] = u.score
        else
          user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
        end
      end
    end

    ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

    number_of_users = ordered_user_scores.length

    percentiles = Hash.new
    i = 1.0
    ordered_user_scores.each_pair do |k,v|
      percentile = (i / number_of_users) * 100
      aux = UserSetting.where(user_id: k).first
      if aux.nil?
        #nothing to do here
      else
        aux.update!(percentile: percentile)
      end
      i = i + 1
    end
  end

end