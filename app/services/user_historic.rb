class UserHistoric
  require 'benchmark'
  #TODO: Sidekiq
  include Sidekiq::Worker # can be used if we want to use a worker
  #
  BASECAMP_EVENT_ID = 1

  def perform
    rebuild_from_historic
  end

  def rebuild_from_historic
    PendingFeedbackReport.all.each do |pfr|
      if ((rebuild_user_question pfr.feedback.id) && (rebuild_user_category pfr.feedback.id))
        pfr.destroy
      end
    end
  end

  def rebuild_user_question reported_feedback_id
    begin
      reported_feedback = Feedback.find(reported_feedback_id)
      #user_category = reported_feedback.user_category
      user_questions = reported_feedback.user_questions
      user_questions.each do |uq|
        previous_user_questions = UserQuestion.where('id < ? and user_id = ? and question_id = ?', uq.id, uq.user.id, uq.question.id).order('id desc')
        previous_user_question =
            previous_user_questions.any? ? previous_user_questions.first : nil
        next_user_questions = UserQuestion.where('id > ? and user_id = ? and question_id = ?', uq.id, uq.user.id, uq.question.id).order('id asc')
        if previous_user_question
          number_of_previous_questions = previous_user_questions.count
          puqs = previous_user_question.score
        else
          number_of_previous_questions = 0
          puqs = 0.0
        end
        next_user_questions.each do |nuq|
          #puts '3'
          nuq.score =
              (puqs * number_of_previous_questions + nuq.feedback.feedback_question_scores.find_by(question_id:uq.question_id).score) / ( number_of_previous_questions + 1)
          #puts '4'
          nuq.save
          number_of_previous_questions = number_of_previous_questions + 1
          puqs = nuq.score
          #puts '5'
        end
        uq.destroy
      end
      return true
    rescue Exception => ex
      puts "1 An error of type #{ex.class} happened, message is #{ex.message}"
      return false
    end
  end

  def rebuild_user_category reported_feedback_id

    begin
      reported_feedback = Feedback.find(reported_feedback_id)
      user_category = reported_feedback.user_category
      previous_user_categories = UserCategory.where('id < ? and user_id = ? and category_id = ?', user_category.id, user_category.user.id, user_category.category.id).order('id desc')
      previous_user_category =
          previous_user_categories.any? ? previous_user_categories.first : nil
      next_user_categories = UserCategory.where('id > ? and user_id = ? and category_id = ?', user_category.id, user_category.user.id, user_category.category.id).order('id asc')
      if previous_user_category
        number_of_previous_categories = previous_user_categories.count
        pucos = previous_user_category.overall_score
      else
        number_of_previous_categories = 0
        pucos = 0.0
      end
      next_user_categories.each do |nuc|
        sum = 0
        question_count = 0
        nuc.feedback.feedback_question_scores.each do |nucfqs|
          sum = sum + nucfqs.score
          question_count = question_count +1
        end
        nuc.overall_score =
            (pucos * number_of_previous_categories + (sum / question_count)) / (number_of_previous_categories + 1)
        nuc.save
        number_of_previous_categories = number_of_previous_categories + 1
        pucos = nuc.overall_score
      end
      user_category.destroy
      return true
    rescue Exception => ex
      puts "2 An error of type #{ex.class} happened, message is #{ex.message}"
      return false
    end
  end

  def next_overall_score
    category_question_count = Category.find(@category_id).questions.count
    (previous_overall_score * existing_categories.count + (@questions_combined_score / category_question_count)) / ( existing_categories.count + 1)
  end

  def self.update_question_percentile_batch_job
    Question.all.each do |question|
      if question.category.id == BASECAMP_EVENT_ID
        next
      end
      user_scores = Hash.new
      question.feedback_question_scores.all.each do |u|
        if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
          next
        end
        if user_scores[u.feedback.receiver.id].nil?
          user_scores[u.feedback.receiver.id] = u.score
        else
          user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
        end
      end

      ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

      number_of_users = ordered_user_scores.length

      percentiles = Hash.new
      i = 1.0
      ordered_user_scores.each_pair do |k,v|
        percentile = (i / number_of_users) * 100
        aux = UserQuestionPercentile.where(user_id: k, question_id: question.id).first
        if aux.nil?
          UserQuestionPercentile.create(user_id: k, question_id: question.id, percentile: percentile)
        else
          aux.update!(percentile: percentile)
        end
        i = i + 1
      end
    end
  end

  def self.update_category_percentile_batch_job
    result = Hash.new
    Category.all.each do |category|
      if category.id == BASECAMP_EVENT_ID
        next
      end
      user_scores = Hash.new
      Question.where(category_id: category.id).each do |question|
        question.feedback_question_scores.all.each do |u|
          if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
            next
          end
          #puts u.feedback.receiver.id
          if user_scores[u.feedback.receiver.id].nil?
            user_scores[u.feedback.receiver.id] = u.score
          else
            user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
          end
        end
      end

      ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

      number_of_users = ordered_user_scores.length

      percentiles = Hash.new
      i = 1.0
      ordered_user_scores.each_pair do |k,v|
        percentile = (i / number_of_users) * 100
        aux = UserCategoryPercentile.where(user_id: k, category_id: category.id).first
        if aux.nil?
          UserCategoryPercentile.create(user_id: k, category_id: category.id, percentile: percentile)
        else
          aux.update!(percentile: percentile)
        end
        i = i + 1
      end
    end
  end

  def self.update_overall_percentile_batch_job
    result = Hash.new
    user_scores = Hash.new
    Question.all.each do |question|
      if question.category.id == BASECAMP_EVENT_ID
        next
      end
      question.feedback_question_scores.all.each do |u|
        if u.feedback.reported || (u.feedback.receiver.id == u.feedback.sender.id)
          next
        end
        #puts u.feedback.receiver.id
        if user_scores[u.feedback.receiver.id].nil?
          user_scores[u.feedback.receiver.id] = u.score
        else
          user_scores[u.feedback.receiver.id] = user_scores[u.feedback.receiver.id] + u.score
        end
      end
    end

    ordered_user_scores = Hash[user_scores.sort_by{|k,v| v}]

    number_of_users = ordered_user_scores.length

    percentiles = Hash.new
    i = 1.0
    ordered_user_scores.each_pair do |k,v|
      percentile = (i / number_of_users) * 100
      aux = UserSetting.where(user_id: k).first
      if aux.nil?
        #nothing to do here
      else
        aux.update!(percentile: percentile)
      end
      i = i + 1
    end
  end

end