class AuthenticationService
  def initialize user_email, auth_token
    @user_email = user_email
    @auth_token = auth_token
  end

  def user
    @user ||= User.find_by_email(@user_email)
  end

  def is_authenticated?
    user_with_auth_token(user)
  end

  private
    def user_with_auth_token user
      user && Devise.secure_compare(user.authentication_token, @auth_token)
    end
end