class UserCategory < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  belongs_to :feedback
end
