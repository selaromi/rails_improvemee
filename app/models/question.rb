class Question < ActiveRecord::Base
  belongs_to :category
  has_many :feedbacks
  has_many :feedback_question_scores
  has_many :user_questions
  default_scope { order('id ASC') }
end
