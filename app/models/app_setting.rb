class AppSetting < ActiveRecord::Base

  def can_send_mail?
    return AppSetting.first.sends_email
  end
end
