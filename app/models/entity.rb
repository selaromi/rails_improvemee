class Entity < ActiveRecord::Base
  validates_presence_of :name, :start_date, :end_date, :entity_type
  has_many :user_entities, dependent: :destroy
  has_many :users, through: :user_entities
  has_many :events, class_name: "Event",foreign_key: "parent_entity"
  has_many :entity_categories
  has_many :categories, through: :entity_categories
  belongs_to :owner, class_name: "User", :foreign_key => :owner
  default_scope { order('id ASC') }
end
