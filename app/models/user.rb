class User < ActiveRecord::Base
  has_one :user_setting
  has_many :feedback_requests
  has_many :solicited_feedback_requests, :foreign_key => 'solicited_id', :class_name => 'FeedbackRequest'
  has_many :user_events
  has_many :user_entities
  has_many :entities, through: :user_entities
  has_many :events, through: :user_events
  has_many :owned_events, :foreign_key => 'owner', :class_name => 'Event'
  has_many :user_customgroups, :foreign_key => 'owner', :class_name => 'UserCustomgroup'
  has_many :owned_entities, class_name: "Entity", :foreign_key => :owner
  default_scope { order('id ASC') }
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  ROLES = %w[admin moderator author banned]

  def admin?
    role == 'admin'
  end

  def organizer?
    role == 'organizer'
  end

  def user?
    role == 'user'
  end

  def acceptable_email?
    email  =~ /[A-Za-z0-9._%+-]+@(cmu.edu|tepper.cmu.edu|andrew.cmu.edu){1}$/
  end

  def reset_authentication_token
    new_token = loop do
      authentication_token = SecureRandom.urlsafe_base64(nil, false)
      break authentication_token unless User.exists?(authentication_token: authentication_token)
    end
    #logger.info "new token created for user #{email}"
    return new_token
  end


  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def visible_events(has_feedback_request = false)
    @events = []
    User.where(role: "organizer").each do |user|
      @events = @events <<  user.owned_events
    end
    if has_feedback_request
      self.solicited_feedback_requests.each do |feedback_request|
        @events = @events <<  feedback_request.event
      end
    end
    @events = @events << self.events
    return @events.flatten!.uniq
  end

  private
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end
end
