class Feedback < ActiveRecord::Base
  validates_presence_of :event_id, :sender_id, :receiver_id
  has_many :feedback_question_scores
  belongs_to :sender, :class_name => "User", :foreign_key => :sender_id
  belongs_to :receiver, :class_name => "User", :foreign_key => :receiver_id
  belongs_to :category
  belongs_to :event
  has_one :pending_feedback_report
  has_one :user_category
  has_many :user_questions
  has_one :report

  def is_anonymous?
    return is_anonymous
  end
end
