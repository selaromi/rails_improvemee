class FeedbackQuestionScore < ActiveRecord::Base
  SCORES = [LOW = 1, LOW_MEDIUM = 2, MEDIUM = 3, MEDIUM_HIGH = 4, MEDIUM_HIGH = 5]

  validates :score, inclusion: {in: SCORES}
  validates_presence_of :feedback_id, :question_id, :score, presence: true
  belongs_to :feedback
  belongs_to :question

end
