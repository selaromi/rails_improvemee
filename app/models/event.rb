class Event < ActiveRecord::Base
  validates_presence_of :name, :start_date, :end_date
  belongs_to :feedback_request
  has_many :user_events
  has_many :users, through: :user_events
  has_many :event_categories
  has_many :categories, through: :event_categories
  belongs_to :owner, class_name: "User", :foreign_key => :owner
  belongs_to :parent_entity, class_name: "Entity", :foreign_key => :parent_entity
  has_many :feedbacks

  def has_parent_entity?
    !parent_entity.nil?
  end

  def save_and_set_users members
    save!
    users.delete_all
    if !members.nil?
    users << User.where(id: members.map { |m| m[:id]})
    end
    #users << parent_entity.users if has_parent_entity?
  end

  def save_and_set_preferred_categories preferred_categories
    save!
    categories.delete_all
    if !preferred_categories.nil?
      categories << Category.where(id: preferred_categories.map { |m| m[:id]})
    end
    #users << parent_entity.users if has_parent_entity?
  end

end
