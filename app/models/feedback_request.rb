class FeedbackRequest < ActiveRecord::Base
  belongs_to :solicitor, :class_name => "User", :foreign_key => :solicitor_id
  belongs_to :solicited, :class_name => "User", :foreign_key => :solicited_id
  belongs_to :event
  belongs_to :category

  # scope :pending,               -> { where(reply: false) }
  scope :pending,               -> { where("status != ? and status != ?", get_status_ignored, get_status_answered) }

  @@NEW_REQUEST = 0
  @@SEEN_REQUEST = 1
  @@IGNORED_REQUEST = 2
  @@ANSWERED_REQUEST = 3

  def FeedbackRequest.pending_by_user user
    pending.where(solicited: user)
  end

  def self.get_status_new
    @@NEW_REQUEST
  end

  def self.get_status_seen
    @@SEEN_REQUEST
  end

  def self.get_status_ignored
    @@IGNORED_REQUEST
  end

  def self.get_status_answered
    @@ANSWERED_REQUEST
  end

  def replied
    if [@@IGNORED_REQUEST, @@ANSWERED_REQUEST].include? self.status
      return true
    end
    return false
  end
end
