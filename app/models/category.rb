class Category < ActiveRecord::Base
  has_many :questions
  has_many :feedbacks
  has_many :user_categories
  default_scope { order('id ASC') }
end
