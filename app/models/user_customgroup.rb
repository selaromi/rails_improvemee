class UserCustomgroup < ActiveRecord::Base
  has_many :user_customgroup_users, dependent: :destroy
  has_many :users, through: :user_customgroup_users
  belongs_to :owner, class_name:"User", :foreign_key => :owner
end
