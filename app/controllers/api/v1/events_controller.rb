class Api::V1::EventsController < ApplicationController
  before_filter :check_permissions, :only => [:new, :create]
  before_filter :authenticate_user!

  respond_to :json

  def create
    @event = Event.new(create_params)
    @event.creator_role = current_user.role
    if !parent_entity_params.nil?
      @event.parent_entity = Entity.find(parent_entity_params)
    end
    if !current_user.organizer?
      @event.owner = current_user
      @event.save!
      @event.users << current_user
    else
      ###Consolidate entity users and event users
      @event.save_and_set_users(members_params)
      @event.save_and_set_users(members_params)
      @event.save_and_set_preferred_categories(preferred_category_params)
    end
    render 'api/v1/events/create', status: :created
  end

  def update
    @event = Event.find(update_params[:id])
    authorize! :update, @event
    @event.update!(update_params)
    #Consolidate entity users and event users if user decides it should
    @event.save_and_set_users(members_params)
    @event.save_and_set_preferred_categories(preferred_category_params)
    render 'api/v1/events/update', status: :ok
  end

  def subscribe
    return render json: { errors: ["No event selected"] }, status: :unprocessable_entity unless list_user_params[:id].presence
    @event = Event.find(subscribe_params[:id])
    current_user.events << @event
    render json: @event, status: :created
  end

  def list_users
    return render json: { errors: ["No event selected"] }, status: :unprocessable_entity unless list_user_params[:id].presence
    authorize! :read, Event.find(list_user_params[:id])
    @users = Event.find(list_user_params[:id]).users
    # @users = @users.select{|user| user if (user_is_not_himself(user)  && (not_exists_previous_feedback(user) || is_test_user?))}
    render 'api/v1/events/list_users'
    # render json: @users, status: :ok
  end

  def get_single_event
    return render json: { errors: ["No event selected"] }, status: :unprocessable_entity unless get_single_event_params[:id].presence
    authorize! :read, Event.find(get_single_event_params[:id])
    @event = Event.find(get_single_event_params[:id])
    render 'api/v1/events/get'
  end

  def all
    @events = Event.all #where('(parent_entity IS NOT NULL AND owner IS NULL) OR owner=?',current_user.id)
    render 'api/v1/events/all'
  end

  def owned
    @events = current_user.owned_entities.first.events
    render 'api/v1/events/all'
  end

  def all_for_current_user
    @events = current_user.events
    render 'api/v1/events/all_for_current_user'
  end

  def all_give_visible_for_current_user
    @events = current_user.visible_events(true)
    render 'api/v1/events/all_for_current_user'
  end

  def all_ask_visible_for_current_user
    @events = current_user.visible_events(false)
    render 'api/v1/events/all_for_current_user'
  end
  
  private
  def create_params
    params.fetch(:event).permit([:name,:description,:start_date,:end_date,:preferred_categories])
  end

  def update_params
    params.fetch(:event).permit([:id,:name,:description,:start_date,:end_date,:preferred_categories])
  end

  def parent_entity_params
    params[:entity]
  end

  def preferred_category_params
    params[:categories]
  end

  def members_params
    params[:members]
  end

  def update_parent_entity_users_params
    params[:update_users].blank? ? 0 : params[:update_users]
  end

  def list_user_params
    params.fetch(:event).permit([:id])
  end

  def get_single_event_params
    params.fetch(:event).permit([:id])
  end

  def list_category_params
    params.fetch(:category).permit([:id])
  end

  def subscribe_params
    params.fetch(:event).permit([:id])
  end

  def should_update_users?
    !update_parent_entity_users_params.zero?
  end

  def check_permissions
    authorize! :create, Event
  end
end