class Api::V1::UserStaticticsController < ApplicationController
  before_filter :authenticate_user!
  
  def categories_score_for_current_user
    @user_categories_scores_ids = UserCategory.where(user_id:current_user.id)
                                          .order(created_at: :desc)
                                          .to_a
                                          .uniq {|user_category| user_category.category_id}
    render json: @user_categories_scores.as_json, status: :ok
  end
  
  def questions_score_for_current_user
    @user_questions_scores = UserQuestion.where(user_id:current_user.id)
                                          .order(created_at: :desc)
                                          .to_a
                                          .uniq {|user_question| user_question.question_id}
    render json: @user_questions_scores.as_json, status: :ok
  end

  def update_percentiles
    UserPercentile.perform_async
  end
  
end