module Api
  module V1
    module CustomDevise
      class RegistrationsController < Devise::RegistrationsController
        prepend_before_filter :require_no_authentication, :only => [ :create ]
        skip_before_filter :authenticate_user_from_token!, :only => [ :create ]
        respond_to :json

        # POST /resource
        def create
          build_resource(sign_up_params)
          resource.role = 'user'
          return render json: { errors: ["Only CMU students for now"] }, status: :unprocessable_entity unless resource.acceptable_email?
          resource.ensure_authentication_token
          if resource.save
            us = UserSetting.new(user_setting_params)
            us.user_id = resource.id
            us.save
            if resource.active_for_authentication?
              sign_up(resource_name, resource)
              render 'api/v1/registrations/create', status: :created
              #render json: {
              #  auth_token: resource.authentication_token,
              #  first_name: resource.first_name,
              #  last_name: resource.last_name,
              #  user_role: resource.role,
              #  nickname: resource.user_settings.nickname
              #}, status: :created
            else
              render json: {errors: [resource.inactive_message]}, status: :created
            end
          else
            clean_up_passwords resource

            render json: {errors: resource.errors.full_messages}, status: :unprocessable_entity
          end
        end

        private

          def sign_up_params
            params.fetch(:user).permit([:password, :password_confirmation, :email, :first_name, :last_name])
          end

          def user_setting_params
            params.fetch(:user_setting).permit([:program_name, :nickname, :graduation_year])
          end

      end
    end
  end
end
