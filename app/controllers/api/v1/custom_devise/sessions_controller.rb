module Api
  module V1
    module CustomDevise
      class SessionsController < Devise::RegistrationsController
        prepend_before_filter :require_no_authentication, :only => [ :create, :destroy ]
        skip_before_filter :authenticate_user_from_token!, :only => [ :create, :destroy ]
        respond_to :json

        def create
          resource = User.find_for_database_authentication({email: params['user']['email']})
          return render json: {errors:["Wrong email or password"]}, status: :unprocessable_entity unless resource.presence && resource.valid_password?(params[:user][:password])
          resource.authentication_token = resource.reset_authentication_token
          return render json: resource.errors, status: :unprocessable_entity unless resource.save!
          @user = resource
          render 'api/v1/sessions/create'
          #render json: {
          #    auth_token: resource.authentication_token,
          #    email: resource.email,
          #}, status: :ok
        end

        #def destroy
        #  current_user.authentication_token = current_user.reset_authentication_token
        #  current_user.save
        #  sign_out(current_user)
        #  render json: { msg: "ok"}, status: :ok
        #end
      end
    end
  end
end