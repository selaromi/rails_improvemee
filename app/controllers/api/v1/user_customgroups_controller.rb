class Api::V1::UserCustomgroupsController < ApplicationController
  before_filter :authenticate_user!

  respond_to :json

  def all
    @user_customgroups = current_user.user_customgroups
    render 'api/v1/user_customgroup/all', status: :ok
  end

  def create
    @user_customgroup = UserCustomgroup.new(create_user_customgroups_params)
    @user_customgroup.owner = current_user
    @user_customgroup.users = User.find(user_customgroup_users_params.split(',')).reject { |r| r == current_user } unless user_customgroup_users_params.blank?
    @user_customgroup.save
    render 'api/v1/user_customgroup/create', status: :created
  end

  def update
    @user_customgroup = UserCustomgroup.find(update_user_customgroups_params[:id])
    authorize! :manage, @user_customgroup
    @user_customgroup.update(update_user_customgroups_params)
    @user_customgroup.users = user_customgroup_users_params.blank? ? [] : User.find(user_customgroup_users_params.split(',')).reject { |r| r == current_user }
    @user_customgroup.save
    render 'api/v1/user_customgroup/update', status: :ok
  end

  def destroy
    @user_customgroup = UserCustomgroup.find(update_user_customgroups_params[:id])
    authorize! :manage, @user_customgroup
    @user_customgroup.destroy
    render json: { msg: 'ok'}, status: :ok
  end

  private
  def create_user_customgroups_params
    params.required(:customgroup).permit(:name, :description)
  end

  def user_customgroup_users_params
    params[:students]
  end

  def update_user_customgroups_params
    params.required(:customgroup).permit(:id,:name, :description)
  end
end
