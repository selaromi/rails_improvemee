class Api::V1::ReportsController < ApplicationController
  before_filter :authenticate_user!

  respond_to :json

  def create
    @feedback = Feedback.find(params[:report][:feedback_id])
    authorize! :read, @feedback
    return render json: {errors: ['Feedback already reported']}, status: :unprocessable_entity if Report.where(report_params).any?
    @report = Report.create(report_params)
    render 'api/v1/reports/create', status: :created
  end

  def solve

  end

  private
    def report_params
      params[:report].permit(:feedback_id)
    end

end