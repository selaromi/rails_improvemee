class Api::V1::UsersController < ApplicationController
  before_filter :authenticate_user!
  skip_before_filter :authenticate_user_from_token!, :only => [:check_token,:change_password, :request_password_reset]
  before_filter :authenticate_user!, :except => [:check_token, :change_password, :request_password_reset]

  respond_to :json

  def check_token
    auth = AuthenticationService.new(user_params[:email],user_params[:auth_token])
    render json: auth.is_authenticated?, status: :ok
  end

  # GET /outlet_types
  def index
    authorize! :read, User
    users =  current_user.admin? ? User.all.order(email: :asc) : [current_user]
    render json: users, status: :ok
  end

  def update_password
    @user = current_user
    return render json: {errors: @user.errors}, status: 422 unless @user.update(user_password_params)
    render 'api/v1/users/update_password', status: :ok
  end

  def all
    @users = User.order("role")
    render 'api/v1/users/all'
    #render json: User.all.as_json(:only => [:id, :first_name, :last_name, :email]), status: :ok
  end

  def all_with_role
    role = all_with_role_params["role_name"]
    @users = User.where(role: role)
    render 'api/v1/users/all'
    #render json: User.all.as_json(:only => [:id, :first_name, :last_name, :email]), status: :ok
  end

  def categories_score_for_current_user
    @user = current_user
    @user_category_scores_ids = UserCategory.where(user_id:current_user.id)
                                          .order(created_at: :desc)
                                          .to_a
                                          .uniq {|user_category| user_category.category_id}
                                          .map { |user_category| user_category.id }

    @user_categories = UserCategory.find(@user_category_scores_ids)
    render 'api/v1/users/categories_score_for_current_user', status: :ok
    #render json: @user_category_scores.as_json(), status: :ok
  end

  def questions_score_for_current_user
    @user_question_scores_ids = UserQuestion.where(user_id:current_user.id)
                                          .order(created_at: :desc)
                                          .to_a
                                          .uniq {|user_question| user_question.question_id}
                                          .map { |user_question| user_question.id }
    @user_questions = UserQuestion.find(@user_question_scores_ids)
    render 'api/v1/users/questions_score_for_current_user', status: :ok
  end

  def questions_percentile_for_current_user
      @user_question_percentiles_ids = UserQuestionPercentile.where(user_id:current_user.id)
                                            .order(created_at: :desc)
                                            .to_a
                                            .uniq {|user_question_percentile| user_question_percentile.question_id}
                                            .map { |user_question_percentile| user_question_percentile.id }
      @user_question_percentiles = UserQuestionPercentile.find(@user_question_percentiles_ids)
      render 'api/v1/users/questions_percentile_for_current_user', status: :ok

    #render json: @user_questions_scores.as_json(), status: :ok
  end

  def categories_percentile_for_current_user
        @user_category_percentiles_ids = UserCategoryPercentile.where(user_id:current_user.id)
                                              .order(created_at: :desc)
                                              .to_a
                                              .uniq {|user_category_percentile| user_category_percentile.category_id}
                                              .map { |user_category_percentile| user_category_percentile.id }
        @user_category_percentiles = UserCategoryPercentile.find(@user_category_percentiles_ids)
        puts @user_category_percentiles.inspect
        render 'api/v1/users/categories_percentile_for_current_user', status: :ok

      #render json: @user_questions_scores.as_json(), status: :ok
  end

  def request_password_reset
    email = request_password_reset_params["email"]
    user_setting = UserSetting.find_by_secondary_email(email)
    if !user_setting.nil?
      user = user_setting.user
    elsif
    user  = User.find_by(email: email)
    end
    if(user)
      user.reset_password_token = SecureRandom.urlsafe_base64(nil, false)
      user.save
      url = ENV['CLIENT_HOST_URL'] + ENV['CLIENT_PASSWORD_RESET_URL'] + user.reset_password_token
      UserMailer.password_reset_email(user, url).deliver
      return render json:{}, status: :ok
    else
      return render json:{errors:'No password reset request for user-'}, status: :bad_request
    end
    return render json:{}, status: :bad_request
  end

  def change_password
    email      = change_password_params["email"]
    resetToken    = change_password_params["reset_password_token"]
    newPassword   = change_password_params["password"]
    user          = (UserSetting.find_by_secondary_email(email) ? UserSetting.find_by_secondary_email(email).user : nil) ||
                      User.find_by(email: email)
    if(user)
      if(user.reset_password_token == resetToken)
        user.password = newPassword
        user.reset_password_token = nil
        if(user.save)
          return render json: {
              username: email
          }, status: :ok
        end
        return render json:{}, status: :bad_request
      end
    else
      return render json:{}, status: :bad_request
    end
    return render json:{}, status: :bad_request
  end


  private
  def user_password_params
    # NOTE: Using `strong_parameters` gem
    params.required(:user).permit(:password, :password_confirmation)
  end

  def request_password_reset_params
    params.require(:user).permit(:email)
  end

  def change_password_params
    params.require(:user).permit(:email, :reset_password_token, :password)
  end

  def all_with_role_params
    params.require(:role).permit(:role_name)
  end
end
