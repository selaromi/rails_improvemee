class Api::V1::FeedbacksController < ApplicationController
  before_filter :authenticate_user!

  respond_to :json

  def create
    return create_from_feedback_request if feedback_request_params.presence

    begin
      User.find(feedback_params[:receiver_id])
    rescue Exception
      return render json: { errors: ["Receiver does not exist"] }, status: :unprocessable_entity
    end

    begin
      Category.find(feedback_params[:category_id])
    rescue Exception
      return render json: { errors: ["Category does not exist"] }, status: :unprocessable_entity
    end

    begin
      Event.find(feedback_params[:event_id])
    rescue Exception
      return render json: { errors: ["Event does not exist"] }, status: :unprocessable_entity
    end

    return render json: { errors: ['all questions must be answered']}, status: :unprocessable_entity unless answers.count ==  Category.find(feedback_params[:category_id]).questions.length
    return render json: { errors: ["Feedback already given"] }, status: :unprocessable_entity if previous_feedback_exists?

    begin
    @feedback = Feedback.create(
        sender_id:current_user.id,
        receiver_id:feedback_params[:receiver_id],
        category_id:feedback_params[:category_id],
        event_id:feedback_params[:event_id],
        is_anonymous:feedback_params[:is_anonymous],
        comments: feedback_comment_params
    )
    rescue Exception
      return render json: { errors: ["Error saving feedback"] }, status: :unprocessable_entity
    end

    # the statistics will be updated only if the feedback is not a self assessment
    if current_user.id != feedback_params[:receiver_id].to_i
      UserStatistics.perform_async(@feedback.category_id,@feedback.id,@feedback.receiver_id,answers) #-- for workers with sidekiq
      # UserStatistics.perform(@feedback.category_id,@feedback.id,@feedback.receiver_id,answers)
    end
    render json: {msg:'ok'}, status: :created
    #render json: 'api/v1/feedbacks/create', status: :created
  end


  def find
    @feedback = Feedback.find(find_feedback_params[:id])
    authorize! :read, @feedback
    @user = current_user
    render 'api/v1/feedbacks/find', status: :ok
  end

  def find_by_sender
    user = find_by_sender_params["sender_id"]
    @feedbacks = Feedback.where(sender_id: user).order(created_at: :desc)
    puts @feedback.inspect
    @user = current_user
    #authorize! :read, @feedback
    render 'api/v1/feedbacks/all_for_given_sender', status: :ok
  end

  def previous_feedback_exists?
    Feedback.where(
        event_id:feedback_params[:event_id],
        sender_id:current_user.id,
        receiver_id:feedback_params[:receiver_id],
        category_id:feedback_params[:category_id],
    ).any?
  end

  def create_from_feedback_request
    return render json: { errors: ["No feedback request selected"] }, status: :unprocessable_entity unless feedback_request_params[:id].presence
    feedback_request = FeedbackRequest.find(feedback_request_params[:id])

    return render json: { errors: ["Insufficient Priviledges"] }, status: :forbidden unless feedback_request.solicited_id == current_user.id
    return render json: { errors: ["Feedback already given"] }, status: :unprocessable_entity if feedback_request.replied
    return render json: { errors: ['all questions must be answered']}, status: :unprocessable_entity unless answers.count == Category.find(feedback_request.category_id).questions.length

    begin
      User.find(feedback_request.solicitor_id)
    rescue Exception
      return render json: { errors: ["Receiver does not exist"] }, status: :unprocessable_entity
    end

    begin
      Category.find(feedback_request.category_id)
    rescue Exception
      return render json: { errors: ["Category does not exist"] }, status: :unprocessable_entity
    end

    begin
      Event.find(feedback_request.event_id)
    rescue Exception
      return render json: { errors: ["Event does not exist"] }, status: :unprocessable_entity
    end

    begin
      @feedback = Feedback.create!(
          feedback_request_id:feedback_request.id,
          sender_id:feedback_request.solicited_id,
          receiver_id:feedback_request.solicitor_id,
          category_id:feedback_request.category_id,
          event_id:feedback_request.event_id,
          is_anonymous:feedback_request_params[:is_anonymous],
          comments: feedback_comment_params
      )
    rescue ActiveRecord::RecordInvalid
      return render json: { errors: ["Problem creating feedback"] }, status: :unprocessable_entity
    end

    feedback_request.update!(status: FeedbackRequest.get_status_answered)

    UserStatistics.perform_async(@feedback.category_id,@feedback.id,@feedback.receiver_id,answers) #-- for workers with sidekiq
    #@feedback_question_scores = FeedbackQuestionScore.where(feedback_id:@feedback.id)
    render json: {msg:'ok'}, status: :created
    #render json: 'api/v1/feedbacks/create', status: :created
    #user_statistics = UserStatistics.new(feedback_request.category_id,@feedback.id,@feedback.receiver_id,answers)
    #user_statistics.update
    #
    ##TODO: UserStatistics.perform_async(feedback_request.category_id,@feedback.id,@feedback.receiver_id,answers) -- for workers with sidekiq
    #@feedback_question_scores = FeedbackQuestionScore.where(feedback_id:@feedback.id)
    #render 'api/v1/feedbacks/create', status: :created
    #render json: FeedbackQuestionScore.where(feedback_id:@feedback.id).as_json(:only => [:question_id,:score]), status: :created
  end

  def received_by_criteria
    #//feeback id por categoria string separado por , de los ids
    @event_ids = params[:event_ids].presence ? params[:event_ids].split(',') : []
    @category_ids = params[:category_ids].presence ? params[:category_ids].split(',') : []
    @question_ids = params[:question_ids].presence ? params[:question_ids].split(',') : []

    @category_ids_by_question = Array.new
    @question_ids.each do |question_id|
      @category_ids_by_question.push Question.find(question_id).category_id
    end
    @category_ids_by_question.uniq!

    @category_ids = @category_ids.concat(@category_ids_by_question) if (@category_ids_by_question.presence)

    query_filters = Hash.new
    query_filters[:receiver_id ] = current_user.id
    query_filters[:event_id ] = @event_ids if @event_ids.presence
    query_filters[:category_id ] = @category_ids if @category_ids.presence

    @user = current_user
    @feedbacks = Feedback.where(query_filters).includes(:report).where('reports.approved IS NULL OR reports.approved = FALSE').references(:report).order(created_at: :desc)

    render 'api/v1/feedbacks/received_by_criteria_for_current_user', status: :ok
  end


  def sent_by_criteria
    #//feeback id por categoria string separado por , de los ids
    @event_ids = params[:event_ids].presence ? params[:event_ids].split(',') : []
    @category_ids = params[:category_ids].presence ? params[:category_ids].split(',') : []
    @question_ids = params[:question_ids].presence ? params[:question_ids].split(',') : []

    @category_ids_by_question = Array.new
    @question_ids.each do |question_id|
      @category_ids_by_question.push Question.find(question_id).category_id
    end
    @category_ids_by_question.uniq!

    @category_ids = @category_ids.concat(@category_ids_by_question) if (@category_ids_by_question.presence)

    query_filters = Hash.new
    query_filters[:sender_id ] = current_user.id
    query_filters[:event_id ] = @event_ids if @event_ids.presence
    query_filters[:category_id ] = @category_ids if @category_ids.presence

    @feedbacks = Feedback.where(query_filters).order(created_at: :desc)
    @user = current_user

    render 'api/v1/feedbacks/sent_by_criteria_for_current_user', status: :ok
  end


  def all_for_current_user
    @feedbacks = Feedback.where(receiver_id: current_user.id).order(created_at: :desc)
                                                 #.as_json(:only => [:id,:is_anonymous,:comments,:seen,:category_id,:created_at],
                                                 #         :include => {
                                                 #             :feedback_question_scores => {
                                                 #                 :only => [:question_id, :score]},
                                                 #             :sender => {
                                                 #                 :only => [:id, :first_name, :last_name, :email]
                                                 #             }
                                                 #         })
    @user = current_user
    render 'api/v1/feedbacks/all_for_current_user', status: :ok
    #render json: @feedback, status: :ok
  end

  def unseen_for_current_user
    @feedbacks = Feedback.where(receiver_id: current_user.id, seen: false).order(created_at: :desc)
                                                 #.as_json(:only => [:id,:is_anonymous,:comments,:seen,:category_id,:created_at],
                                                 #         :include => {
                                                 #             :feedback_question_scores => {
                                                 #                 :only => [:question_id, :score]},
                                                 #             :sender => {
                                                 #                 :only => [:id, :first_name, :last_name, :email]
                                                 #             }
                                                 #         })
    @user = current_user
    render 'api/v1/feedbacks/unseen_for_current_user', status: :ok
  end


  def sent_by_current_user
    @feedbacks = Feedback.where(sender_id: current_user.id).order(created_at: :desc)
    #.as_json(:only => [:id,:is_anonymous,:comments,:seen,:category_id,:created_at],
    #         :include => {
    #             :feedback_question_scores => {
    #                 :only => [:question_id, :score]},
    #             :sender => {
    #                 :only => [:id, :first_name, :last_name, :email]
    #             }
    #         })
    @user = current_user
    render 'api/v1/feedbacks/sent_by_current_user', status: :ok
    #render json: @feedback, status: :ok
  end

  def feedbacks_by_event
    @feedbacks = Feedback.where(event_id: feedbacks_by_event_params[:event_id], category_id: feedbacks_by_event_params[:category_id])
    puts @feedbacks.inspect
    render 'api/v1/feedbacks/sent_by_event', status: :ok
  end

  def set_seen
      return render json: {errors: 'No feedback selected'}, status: :unprocessable_entity unless set_seen_feedback_params[:id].presence
      @feedbacks = Feedback.where(id: set_seen_feedback_params[:id], receiver_id: current_user.id).first
      if @feedbacks.nil?
        return render json: {errors: 'No feedback with this id'}, status: :unprocessable_entity
      else

        @feedbacks.update!(seen: (set_seen_feedback_params.has_key? :seen) ? set_seen_feedback_params[:seen] : true );
      end
      render 'api/v1/feedbacks/set_seen', status: :ok
  end

  def has_self_assessment
    @feedbacks = Feedback.where(category_id: has_self_assessment_params[:category_id], sender_id: current_user.id, receiver_id: current_user.id)
    @user = current_user
    render 'api/v1/feedbacks/all_for_current_user', status: :ok
  end

  private
    def answers
      feedback_question_score_params.split(',')
    end
    def feedback_request_params
      params.fetch(:feedback_request).permit([:id,:is_anonymous]) if params[:feedback_request].presence
    end

    def feedback_question_score_params
      params[:feedback_question_score]
    end

    def feedback_comment_params
      params[:feedback_comments]
    end

    def feedback_params
      params[:feedback]
    end

    def set_seen_feedback_params
      params.fetch(:feedback).permit([:id,:seen])
    end

    def find_feedback_params
      params.fetch(:feedback).permit([:id])
    end

    def find_by_sender_params
      params.fetch(:feedback).permit([:sender_id])
    end

    def feedbacks_by_event_params
      params.fetch(:feedback).permit(:event_id, :category_id)
    end

    def has_self_assessment_params
      params.fetch(:feedback).permit(:category_id)
    end
end