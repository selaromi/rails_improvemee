class Api::V1::QuestionsController < ApplicationController
  before_filter :authenticate_user!

  respond_to :json
  def index
    @questions = Question.all.order(id: :asc)
    render json: @questions.as_json, status: :ok
  end

  def all_by_category
    @questions = Question.where(category_id: all_by_category_params[:id]).order(id: :asc)
    render 'api/v1/questions/all_by_category'
    #render json: @questions.as_json, status: :ok
  end

  def plot_data
    question_id = params[:id].to_i
    category_id = params[:category_id].to_i
    data_plot = []
    if question_id==0
      data_plot = UserCategory.where(user_id: current_user.id).where(category_id: category_id).last(30).each_with_index.map {|uc,i| [i+1,uc.overall_score]}
    else
      data_plot = UserQuestion.where(user_id: current_user.id).where(question_id: question_id).last(30).each_with_index.map {|uq,i| [i+1,uq.score]}
    end
      return render json: data_plot.to_json

  end

  private
    def all_by_category_params
      params.fetch(:category).permit([:id])
    end
end