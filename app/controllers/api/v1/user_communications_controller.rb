class Api::V1::UserCommunicationsController < ApplicationController
  before_filter :authenticate_user!

  SEND_FEEDBACK_REQUEST_REMINDER_TYPE = 2
  SEND_FEEDBACK_RECEIVED_REPORT_TYPE = 3

  respond_to :json

  def all
    @user_communications = UserCommunication.where(user_id:current_user.id)
    render 'api/v1/user_communications/all', status: :ok
  end

  def update
    @user_communication = UserCommunication.find_or_initialize_by(user_id: current_user.id, communication_id: SEND_FEEDBACK_REQUEST_REMINDER_TYPE)
    @user_communication.update(frequency:update_user_communication_params[:requestFeedbackFrequency])
    @user_communication = UserCommunication.find_or_initialize_by(user_id: current_user.id, communication_id: SEND_FEEDBACK_RECEIVED_REPORT_TYPE)
    @user_communication.update(frequency:update_user_communication_params[:receivedFeedbackFrequency])
    render json: 'ok', status: :ok
  end



  private
  def update_user_communication_params
    params.required(:communications).permit(:receivedFeedbackFrequency,:requestFeedbackFrequency)
  end
end
