class Api::V1::FeedbackRequestsController < ApplicationController
  before_filter :authenticate_user!

  respond_to :json

  def pending
    @feedback_requests = FeedbackRequest.pending_by_user(current_user)
    render 'api/v1/feedback_requests/pending', status: :ok
    #render json: FeedbackRequest.pending_by_user(current_user).as_json, status: :ok
  end

  def direct
    return render json: {errors: ['No feedback category selected']}, status: :unprocessable_entity unless direct_request_params[:categories_ids].presence
    @feedback_requests_ids = Array.new
    direct_request_params[:solicited_ids].split(',').map { |solicited_id|
      direct_request_params[:categories_ids].split(',').map { |category_id|
        next if FeedbackRequest.where(solicitor_id:current_user.id, solicited_id:solicited_id, category_id: category_id, event_id:direct_request_params[:event_id]).any?
        feedback_request = FeedbackRequest.create(solicitor_id:current_user.id, solicited_id:solicited_id, category_id: category_id, event_id:direct_request_params[:event_id],status:FeedbackRequest.get_status_new)
        @feedback_requests_ids.push(feedback_request.id)
      }
    }
    @feedback_requests = FeedbackRequest.find(@feedback_requests_ids)
    render 'api/v1/feedback_requests/direct', status: :created
    #render json: @created_feedback_requests.as_json(), status: :created
  end

  def requests_by_event
    @feedback_requests = FeedbackRequest.where(event_id:requests_by_event_params[:event_id], category_id: requests_by_event_params[:category_id])
    render 'api/v1/feedback_requests/sent_by_event', status: :ok
  end

  def set_requests_as_seen
    feedback_request = FeedbackRequest.find(set_requests_as_seen_params[:id])
    if feedback_request.solicited_id != current_user.id
      return render json: { errors: ["Problem setting feedback request"] }, status: :unprocessable_entity
    end
    feedback_request.update!(status: FeedbackRequest.get_status_seen)
    render json: {msg:'ok'}, status: :created
  end

  def set_requests_as_ignored
    feedback_request = FeedbackRequest.find(set_requests_as_seen_params[:id])
    if feedback_request.solicited_id != current_user.id
      return render json: { errors: ["Problem setting feedback request"] }, status: :unprocessable_entity
    end
    feedback_request.update!(status: FeedbackRequest.get_status_ignored)
    render json: {msg:'ok'}, status: :created
  end

  private
    def direct_request_params
      params.fetch(:feedback_request).permit([:event_id,:categories_ids, :solicited_ids])
    end

    def requests_by_event_params
      params.fetch(:feedback_request).permit(:event_id, :category_id)
    end

    def set_requests_as_seen_params
      params.fetch(:feedback_request).permit(:id)
    end

    def set_requests_as_ignored_params
      params.fetch(:feedback_request).permit(:id)
    end


end