class Api::V1::UserSettingsController < ApplicationController
  #before_filter :authenticate_user!

  respond_to :json

  def update
    current_user.user_setting ||= UserSetting.create!
    current_user.user_setting.update(user_setting_params)
    @user_setting = current_user.user_setting
    render 'api/v1/user_settings/update', status: :ok
  end

  private
    def user_setting_params
      params.fetch(:user_settings).permit([:program_name, :nickname, :graduation_year, :secondary_email, :opt_out, :licence_agreement])
    end
end