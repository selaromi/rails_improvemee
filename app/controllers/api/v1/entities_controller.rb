class Api::V1::EntitiesController < ApplicationController
  before_filter :check_permissions, :only => [:new, :create]
  before_filter :authenticate_user!

  respond_to :json

  def create
    @entity = Entity.new(create_params)
    @entity.owner = current_user
    @entity.users = User.find(user_entity_params.split(',')).reject { |r| r == current_user } unless user_entity_params.blank?
    render json: @entity.errors, status: :unprocessable_entity unless @entity.save!
    render 'api/v1/entities/create', status: :created

    # render json: @entity.as_json(:only => [:id,:name,:description,:start_date,:end_date,:preferred_categories],
                                # :include => {
                                #     :owner => {
                                #         :only => [:id, :email]}
                                # }), status: :created
  end

  def update
    @entity = Entity.find(update_params[:id])
    authorize! :update, @entity
    @entity.users = user_entity_params.blank? ? [] : User.find(user_entity_params.split(',')).reject { |r| r == current_user }
    @entity.update!(update_params)
    render 'api/v1/entities/update', status: :ok
    # render json: @entity.as_json, status: :ok
  end

  def update_users

  end


  def owned
    @entities = current_user.owned_entities
    render 'api/v1/entities/owned'
  end

  def destroy
    @entity = Entity.find(destroy_params[:id])
    authorize! :manage, @entity
    return render json: { errors: ["This Entity has feedbacks associated to it and cannot be deleted"] }, status: :unprocessable_entity if Feedback.where(id:@entity.events.pluck(:id)).any?
    @entity.destroy
    render json: {}, status: :ok
    # render json: @entity.as_json, status: :ok
  end

  def list_users
    return render json: { errors: ["No entity selected"] }, status: :unprocessable_entity unless list_user_params[:id].presence 
    authorize! :read, Entity.find(list_user_params[:id])
    @entity = Entity.find(list_user_params[:id])
    @users_entities  = @entity.user_entities
    render 'api/v1/entities/list_users', status: :ok
    # @users = @users.select{|user| user if (user_is_not_himself(user)  && (not_exists_previous_feedback(user) || is_test_user?))}
    # render json: @users, status: :ok
  end

  def not_exists_previous_feedback user
    Feedback.where(sender_id: current_user.id, receiver_id: user["id"], event_id: list_user_params[:id], category_id: list_category_params[:id]).empty?
  end

  def all
    @entities = Entity.all
    render 'api/v1/entities/all'
    # render json: @events.as_json(:except => [:updated_at, :created_at] ), status: :ok
  end

  def all_for_current_user
    @events = current_user.events
    render json: @events.as_json(:except => [:updated_at, :created_at] ), status: :ok
  end
  
  private
  def list_user_params
    params.fetch(:entity).permit([:id])
  end

  def subscribe_params
    params.fetch(:entity).permit([:id])
  end

  def create_params
    params.fetch(:entity).permit([:name,:description,:start_date,:end_date,:preferred_categories, :entity_type])
  end

  def user_entity_params
    params[:students]
  end

  def update_params
    params.fetch(:entity).permit([:id,:name,:description,:start_date,:end_date,:preferred_categories, :entity_type])
  end

  def destroy_params
    params.fetch(:entity).permit([:id])
  end

  def check_permissions
    authorize! :manage, Entity

  end

  def check_administration_permissions entity
    authorize! :manage, entity

  end
end