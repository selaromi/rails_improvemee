class Api::V1::SelfAssessmentsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @self_assessment = SelfAssessment.new(user_id:current_user.id, category_id: self_assessment_params[:category_id])
    @self_assessment.score = self_assessment_params[:score]
    if @self_assessment.save
      return render json: @self_assessment.as_json, status: :ok
    else
      return render json: [].as_json, status: :ok
    end
  end

  def index
    @self_assessments = SelfAssessment.where(user_id:current_user.id, category_id: self_assessment_params[:category_id])
    render json: @self_assessments, status: :ok
  end

  private
    def self_assessment_params
      params.fetch(:self_assessment).permit([:category_id, :score])
    end
  
end