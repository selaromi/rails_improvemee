class Api::V1::CategoriesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  def index
    @categories = Category.all.order(id: :asc)
    render 'api/v1/categories/index', status: :ok
  end
end