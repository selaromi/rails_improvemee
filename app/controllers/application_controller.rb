class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::StrongParameters
  include CanCan::ControllerAdditions

  before_filter :authenticate_user_from_token!, :authenticate_user!, :cors_preflight_check
  after_filter :cors_set_access_control_headers


  #Handle authorization exception from CanCan
  rescue_from CanCan::AccessDenied do |exception|
    render json: {errors: ["Insufficient privileges"]}, status: :forbidden
  end

  #Handle RecordNotFound errors
  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: {errors: [exception.message]}, status: :unprocessable_entity
  end

  rescue_from(ActionController::ParameterMissing) do |parameter_missing_exception|
    error = {}
    error[parameter_missing_exception.param] = ['parameter is required']
    response = { errors: [error] }
    respond_to do |format|
      format.json { render json: response, status: :unprocessable_entity }
    end
  end

  rescue_from(ActiveRecord::RecordInvalid) do |record_invalid_exception|
    respond_to do |format|
      format.json { render json: record_invalid_exception.record.errors, status: :unprocessable_entity }
    end
  end

  def wrong_route
    render json: {errors: ['Hey! Where are you going?']}, status: :unauthorized
  end
  
  private
    def authenticate_user_from_token!
      return render json: {errors: ["The signature is wrong (are you missing out any fields?)"]}, status: :unprocessable_entity   unless (user_params.presence && user_params[:email].presence && user_params[:auth_token].presence)
      auth = AuthenticationService.new(user_params[:email],user_params[:auth_token])
      if auth.is_authenticated?
        logger.info "#{auth.user.email} authorized"
        sign_in auth.user, store: false
      else
        render json: {errors: ["Invalid login"]}, status: :unauthorized
      end
    end

    def user_params
      params[:user]
    end

    def cors_set_access_control_headers
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
      headers['Access-Control-Allow-Headers'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
      headers['Access-Control-Max-Age'] = "1728000"
    end

    # If this is a preflight OPTIONS request, then short-circuit the
    # request, return only the necessary headers and return an empty
    # text/plain.

    def cors_preflight_check
      if request.method == :options
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
        headers['Access-Control-Request-Method'] = '*'
        headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        render :text => '', :content_type => 'text/plain'
      end
    end

end
