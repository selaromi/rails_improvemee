# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

#use whenever -w to start the cron jobs
#and whenever --update-crontab --set environment='development' to execute in dev env
#use whenever clear to stop them

every 1.minutes do
  runner "UserSetting.update_question_percentile_batch_job", :output => '/Users/carloscrespo/Documents/railsProjects/rails_improvemee/cron.log'
end

every 1.minutes do
  runner "UserSetting.update_category_percentile_batch_job", :output => '/Users/carloscrespo/Documents/railsProjects/rails_improvemee/cron.log'
end

every 1.minutes do
  runner "UserSetting.update_overall_percentile_batch_job", :output => '/Users/carloscrespo/Documents/railsProjects/rails_improvemee/cron.log'
end
