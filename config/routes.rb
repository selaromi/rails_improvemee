require 'api_constraints'

Rails4Api::Application.routes.draw do

  scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
    devise_for :users, defaults: {format: :json}, path: '/api/users',controllers: {
      registrations: 'api/v1/custom_devise/registrations',
      sessions: 'api/v1/custom_devise/sessions'

    }
  end

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do

      #resources :users, only: [:nothing] do
        resources :categories, only: [:nothing] do
          resources :self_assessments, only: [:create] do
            collection do
              post "index"
            end
          end
          resources :questions, only: [:nothing] do
            member do
              post "plot_data"
            end
          end
        end
      #end




      #User
      post 'user/check_token' => 'users#check_token'

      #Student
      post 'students/list' => 'users#all' #
      post 'students/listwithrole' => 'users#all_with_role' #
      put  'student/settings' => 'user_settings#update' #
      put 'student/password' => 'users#update_password' #
      post 'student/forgotpass'  => 'users#request_password_reset'
      post 'student/resetpass'  => 'users#change_password'

      #Category & Questions
      post 'questions' => 'questions#index' # No tiene mucho sentido solo
      post 'categories' => 'categories#index' #
      post 'categories/questions' => 'questions#all_by_category' #
      post 'categories/scores' => 'users#categories_score_for_current_user' #
      post 'questions/scores' => 'users#questions_score_for_current_user' #
      post 'questions/percentiles' => 'users#questions_percentile_for_current_user' #
      post 'categories/percentiles' => 'users#categories_percentile_for_current_user' #
      post 'category/questions/scores' => 'users#questions_score_by_category_for_current_user'

      #Feedback
      post 'feedback/find' => 'feedbacks#find' #
      post 'feedback/findbysender' => 'feedbacks#find_by_sender' #
      post 'feedback/request' => 'feedback_requests#direct' #
      post 'feedback/pending' => 'feedback_requests#pending' #
      post 'feedback' => 'feedbacks#create' #
      post 'feedback/received/unchecked' => 'feedbacks#unseen_for_current_user' #
      post 'feedback/received' => 'feedbacks#received_by_criteria' #
      post 'feedback/sent' => 'feedbacks#sent_by_criteria' #
      post 'feedback/findbyevent' => 'feedbacks#feedbacks_by_event' #
      post 'feedback/selfassessment' => 'feedbacks#has_self_assessment' #
      put  'feedback/seen' => 'feedbacks#set_seen' #


      #FeedbackRequest
      post  'feedbackrequests/findbyevent' => 'feedback_requests#requests_by_event' #
      post  'feedbackrequests/setasseen' => 'feedback_requests#set_requests_as_seen' #
      post  'feedbackrequests/setasignored' => 'feedback_requests#set_requests_as_ignored' #

      #Report
      post 'feedback/report' => 'reports#create' #

      #User Custom Groups
      post 'student/custom_groups' => 'user_customgroups#all' #
      post 'student/custom_group' => 'user_customgroups#create' #
      put  'student/custom_group' => 'user_customgroups#update' #
      delete 'student/custom_group' => 'user_customgroups#destroy' #

      #User Communications
      post 'student/communications' => 'user_communications#all'
      put 'student/communications' => 'user_communications#update'

      #Entity
      post 'entity' => 'entities#create' #
      put  'entity' => 'entities#update' #
      post 'entity/students/list' => 'entities#list_users' #
      post 'entities' => 'entities#all' #
      post 'entities/owned' => 'entities#owned' #
      delete 'entity' => 'entities#destroy' #

      #Event
      post 'event' => 'events#create' #
      put  'event' => 'events#update' #
      post 'events' => 'events#all' #
      post 'student/events/give' => 'events#all_give_visible_for_current_user' #
      post 'student/events/ask' => 'events#all_ask_visible_for_current_user' #
      post 'student/events' => 'events#all_for_current_user' #
      post 'events/owned' => 'events#owned' #
      post 'event/getsingle' => 'events#get_single_event' #
      post 'event/students' => 'events#list_users' #
      post 'event/subscribe' => 'events#subscribe'

      #Batch jobs
      post 'percentiles/calculate' => 'user_statistics#update_percentiles'

    end
  end


  get 'loaderio-08ea0f3bb98ed543fb48219afbcd048b/', :to => redirect('/loaderio-08ea0f3bb98ed543fb48219afbcd048b.txt')
  match "*path" => 'application#wrong_route', via: [:get, :post, :delete]
  #root :to => "home#index"
end
