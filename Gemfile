source 'https://rubygems.org'
ruby '2.0.0'

gem 'rails', '4.0.0'
gem 'rails-api' #Rails on API mode
gem 'rqrcode'
gem 'pg'
gem 'warden', '1.2.3'
gem 'devise'
gem 'cancan' #For authorization
gem 'active_model_serializers'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0.rc1'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
gem 'rabl', '~> 0.10.1'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use unicorn as the app server
gem 'unicorn'
gem 'puma'
gem 'sidekiq'
# Use Capistrano for deployment
# gem 'capistrano', group: :development

gem 'rails_12factor'

# Use debugger
gem 'debugger', group: [:development, :test]

#Testing
gem "rspec-rails" , '~> 3.0.0', :group => [:development, :test] #Unit test framework
gem "database_cleaner"
gem "cucumber-rails", :group => :test, :require => false #Behaviour driven development
gem "factory_girl_rails", :group => [:development, :test] #Factory for DB data
gem "shoulda-matchers", :group => :test #Collection of Rails testing matchers
gem 'cucumber-api-steps','0.10', :require => false, :group => :test #Cucumber steps for API
gem 'json_spec', '~> 1.1.2', group: :test # JSON matchers for tests

#Metrics and Test Coverage
gem 'metric_fu', '~> 4.10.0', :group => :test
gem 'simplecov', :require => false, :group => :test
#gem 'rb-fsevent', :require => false if RUBY_PLATFORM =~ /darwin/i

group :development do
  gem 'guard-cucumber'
end
gem 'rack-cors', :require => 'rack/cors'

gem 'whenever', :require => false
