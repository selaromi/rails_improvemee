namespace :scheduler do
  desc 'update users historic'
    task update_user_historic: :environment do
      puts 'Starting Worker for: Updating historic...'
      UserHistoric.perform_async
      puts 'done.'
    end
end