namespace :scheduler do
  desc 'Send feedback and feedback request emails'
    task send_feedback_reminders: :environment do
      puts 'Starting Worker for: Sending reminders'
      AsyncMailer.delay.send_feedback_received_report
      AsyncMailer.delay.send_feedback_request_reminder
      puts 'done.'
    end
end