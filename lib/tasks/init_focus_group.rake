
namespace :db do
  desc "Clean and fill database with test data"

  task init_focus_group: :environment do

    Category.delete_all
    Question.delete_all
    Event.delete_all

    Category.create(id:1,name:"Presentations",description:"Presentations",overall_score:0)
    Question.create(id:1,category_id:1,text:"Was the speaker's message clear?",overall_score:0)
    Question.create(id:2,category_id:1,text:"Was the speaker engaging/interesting?",overall_score:0)
    Question.create(id:3,category_id:1,text:"Was the speaker's message well structured?",overall_score:0)
    Question.create(id:4,category_id:1,text:"Did the speaker show good body language?",overall_score:0)
    Question.create(id:5,category_id:1,text:"Was the objective of the presentation accomplished?",overall_score:0)
    Category.create(id:2,name:"Team Performance",description:"Team Performance",overall_score:0)
    Question.create(id:6,category_id:2,text:"Was this person reliable? (e.g. met his commitments on time and with high quality)",overall_score:0)
    Question.create(id:7,category_id:2,text:"Did this person provide valuable contributions?",overall_score:0)
    Question.create(id:8,category_id:2,text:"Did this person listen and utilize others ideas/opinions?",overall_score:0)
    Question.create(id:9,category_id:2,text:"Did this person put the expected level of effort into the team’s work?",overall_score:0)
    Question.create(id:10,category_id:2,text:"Would you like to work with this person again on a team?",overall_score:0)
    
    events = [
        {name:'Aussie Air negotiation',description:'Negotiations night class',start_date:Time.new(2014, 04, 28, 18, 30, 0),end_date:Time.new(2014, 04, 28, 22, 00, 0)},
        {name:'Aussie Air negotiation', description:'Negotiations day class',start_date:Time.new(2014, 04, 28, 15, 30, 0),end_date:Time.new(2014, 04, 28, 17, 30, 0)},
        {name:'Presentation Mon',description:'Executive communication skills class',start_date:Time.new(2014, 04, 28, 10, 30, 0),end_date:Time.new(2014, 04, 28, 10, 30, 0)+2.hours},
        {name:'Presentation Wed',description:'Executive communication skills class',start_date:Time.new(2014, 04, 30, 10, 30, 0),end_date:Time.new(2014, 04, 30, 10, 30, 0)+2.hours}
    ]
    events.each do |event|
      Event.create(name:event[:name],description:event[:description],start_date:event[:start_date],end_date:event[:end_date])
    end
    users =
    [
        {first_name:"Connie", last_name:"Chun",	email: "cschun@tepper.cmu.edu", event_id: [1]},
        {first_name:"Andrew", last_name:"Goodman",	email: "andrewgo@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Neel", last_name:"Kishan",	email: "nkishan@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Alexander", last_name:"Melesko",	email: "amelesko@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Divyesh", last_name:"Patel",	email: "divyeshp@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Vignesh", last_name:"Rajamani", email:	"vrajama1@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Christopher", last_name:"Vogt",	email: "christov@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Chintan", last_name:"Desai",	email: "chintand@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Michael", last_name:"Harris",email:	"mharris1@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Ramya", last_name:"Laxman",	email: "rlaxman@tepper.cmu.edu", event_id: [ 1,3,4]},
        {first_name:"Catherine", last_name:"Monk",	email: "cmonk@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Sasha", last_name:"Prabhu",	email: "sashap@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Abiram", last_name:"Satish Sivasankaran",	email: "asivasan@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Kai", last_name:"Wei",	email: "kaiwei@tepper.cmu.edu", event_id: [1]},
        {first_name:"Raymond", last_name:"Fan",	email: "raymondf@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Robert", last_name:"Kim",	email: "robertki@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Sakshi", last_name:"Mehta",	email: "sakshim@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Moises", last_name:"Morgenstern Gali",	email: "mmorgens@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Ramesh", last_name:"Babu Radhakrishnan",	email: "Rameshbr@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Samantha", last_name:"Speer",	email: "sspeer@tepper.cmu.edu", event_id: [ 1]},
        {first_name:"Timothy", last_name:"Altman",	email: "taltman@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Chinmayi", last_name:"Bhavanishankar",	email: "cbhavani@tepper.cmu.edu",event_id: [ 2]},
        {first_name:"Tzu-Kang",last_name: "Cheng",	email: "tzukangc@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Yang", last_name:"Fei",	email: "yangf@tepper.cmu.edu", event_id: [ 1,3,4]},
        {first_name:"Christian", last_name:"Hall", email:	"chall1@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Adam", last_name:"Kuhn",	email: "adamkuhn@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Rishi", last_name:"Maharaj",	email: "rmaharaj@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Devlin", last_name:"Robinson",	email: "Devlinr@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Kristen", last_name:"Ballweg",	email: "kballweg@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Michael", last_name:"Bojanowski",	email: "mbojanow@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Michelle", last_name:"Crottier",	email: "mcrottie@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Iskender", last_name:"Humbaraci",	email: "iah@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Al", last_name:"Liang", email:	"aliang1@andrew.cmu.edu", event_id: [ 2]},
        {first_name:"Zohaib", last_name:"Mahmood",	email: "zmahmood@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Dan", last_name:"Spice",	email: "dspice@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Norbert", last_name:"Fritz",	email: "nfritz@andrew.cmu.edu", event_id: [ 2]},
        {first_name:"Jing", last_name:"Bao",	email: "jingbao@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Alexander", last_name:"Brown",	email: "alexandb@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Ashley", last_name:"Dickson",	email: "adickson@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Kevin", last_name:"Gallagher",	email: "kgallagh@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Brigid", last_name:"Johnson",	email: "brigidj@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Shreshth", last_name:"Luthra",	email: "shreshtl@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Miguel", last_name:"Martinez Alanis",	email: "miguelm@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Raghavendra", last_name:"Venkata",	email: "raghavev@tepper.cmu.edu", event_id: [ 2]},
        {first_name:"Kamal", last_name:"Chandwani",		email: "kchandwa@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Richard", last_name:"Davis",			email: "richardd@tepper.cmu.edu", event_id: [3,4]},
        {first_name:"Anjali", last_name:"Gautam",			email: "anjalig@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Kaijie", last_name:"Hu",				email: "kaijieh@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Scott", last_name:"Jacob",			email: "scottj@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Kashyap", last_name:"Malkan",			email: "kmalkan@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Ricky", last_name:"Morris",			email: "rickym@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Stas", last_name:"Neyman",			email: "sneyman@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Shiful", last_name:"Parti",			email: "sparti@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Ahyoung", last_name:"Shin",	email:		"ashin1@tepper.cmu.edu", event_id: [ 3,4]},
        {first_name:"Sutichai", last_name:"Tejapaibul",	email: "stejapai@tepper.cmu.edu", event_id: [ 3,4]}
      ]
    users.each_with_index { |user_info,index|
      @user = User.create(first_name: "#{user_info[:first_name]}",
                last_name: "#{user_info[:last_name]}",
                role: 'user',
                email: "#{user_info[:email]}",
                password:"improveMeeTest#{index+1}"
                )
      UserSetting.create(user_id: @user.id, program_name:"Focus Group",nickname:"#{user_info[:first_name]}")
      user_info[:event_id].each do |event_id|
        UserEvent.create(user_id:@user.id,event_id:event_id)
      end


    }

  end
end

#Dr09Pc8RChODUzFjdzwCmA