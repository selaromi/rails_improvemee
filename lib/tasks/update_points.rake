namespace :scheduler do
  desc 'update users percentile'
    task update_points: :environment do
      puts 'Starting Worker for: Updating percentile...'
      UserPercentile.perform_async
      puts 'done.'
    end
end