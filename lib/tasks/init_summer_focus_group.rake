
namespace :db do
  desc "Clean and fill database with test data for init_summer group"

  task init_summer_focus_group: :environment do
    
    events = [
        {name:'Management Presentations - Tuesday, May 27 first',description:'Management Presentations - Tuesday, May 27 first',start_date:Time.new(2014, 05, 27, 18, 00, 0),end_date:Time.new(2014, 05, 27, 19, 45, 0)},
      {name:'Management Presentations - Tuesday, May 27 second', description:'Management Presentations - Tuesday, May 27 second',start_date:Time.new(2014, 05, 27, 20, 00, 0),end_date:Time.new(2014, 05, 27, 21, 45, 0)},
        {name:'Management Presentations - Wednesday, May 28 first',description:'Management Presentations - Wednesday, May 28 first',start_date:Time.new(2014, 05, 28, 18, 00, 0),end_date:Time.new(2014, 05, 28, 18, 00, 0)+105.minutes},
      {name:'Management Presentations - Wednesday, May 28 second',description:'Management Presentations - Wednesday, May 28 second',start_date:Time.new(2014, 05, 28, 20, 00, 0),end_date:Time.new(2014, 05, 28, 20, 00, 0)+105.minutes}
    ]
    events.each do |event|
      Event.create(name:event[:name],description:event[:description],start_date:event[:start_date],end_date:event[:end_date])
    end
    users =
    [
        {first_name:"Tariq", last_name:"Aftab", email: "taftab@tepper.cmu.edu",event_id: [7]},
        {first_name:"Leonardo F", last_name:"Chaves", email: "lchaves@tepper.cmu.edu",event_id: [7]},
        {first_name:"Robert Francis", last_name:"Downey", email: "rdowney@tepper.cmu.edu",event_id: [7]},
        {first_name:"Tony", last_name:"Efremenko", email: "tefremen@tepper.cmu.edu",event_id: [7]},
        {first_name:"Jacob", last_name:"Hirsch", email: "jacobh@tepper.cmu.edu",event_id: [7]},
        {first_name:"Amy", last_name:"Joy", email: "amyjoy@tepper.cmu.edu",event_id: [7]},
        {first_name:"Brian K", last_name:"Lash", email: "blash@tepper.cmu.edu",event_id: [7]},
        {first_name:"Lauren", last_name:"Moorhead", email: "lmoorhea@tepper.cmu.edu",event_id: [7]},
        {first_name:"Akshay", last_name:"Nemlekar", email: "akshayne@tepper.cmu.edu",event_id: [7]},
        {first_name:"Rodd Justin", last_name:"Pribik", email: "rpribik@tepper.cmu.edu",event_id: [7]},
        {first_name:"Yogin", last_name:"Shroff", email: "yogins@tepper.cmu.edu",event_id: [7]},
        {first_name:"Joseph Daniel", last_name:"Volk", email: "jvolk@tepper.cmu.edu",event_id: [7]},
        {first_name:"Kai", last_name:"Zhang", email: "kaizhang@tepper.cmu.edu",event_id: [7]},
        {first_name:"Ziheng", last_name:"Zhu", email: "zihengz@tepper.cmu.edu",event_id: [7]},
        {first_name:"Aaron Michael", last_name:"Basmajian", email: "ambasmaj@tepper.cmu.edu", event_id: [8]},
        {first_name:"Craig Tyler", last_name:"Cuthbert", email: "ccuthber@tepper.cmu.edu", event_id: [8]},
        {first_name:"Yu", last_name:"Gu", email: "yugu@tepper.cmu.edu", event_id: [8]},
        {first_name:"Laine", last_name:"Guttman", email: "laineg@tepper.cmu.edu", event_id: [8]},
        {first_name:"Drew William", last_name:"Jesse", email: "dwj@tepper.cmu.edu", event_id: [8]},
        {first_name:"Sunny", last_name:"Jhurani", email: "sjhurani@tepper.cmu.edu", event_id: [8]},
        {first_name:"Michael Thomas", last_name:"Porter", email: "mtporter@tepper.cmu.edu", event_id: [8]},
        {first_name:"Saravanan", last_name:"Rajadinakaran", email: "srajadin@tepper.cmu.edu", event_id: [8]},
        {first_name:"Amit", last_name:"Tacker", email: "amitt@tepper.cmu.edu", event_id: [8]},
        {first_name:"Xinrou", last_name:"Tan", email: "xinrout@tepper.cmu.edu", event_id: [8]},
        {first_name:"Michael", last_name:"Yacavino", email: "myacavin@tepper.cmu.edu", event_id: [8]},
        {first_name:"Karen", last_name:"Yu", email: "kareny1@tepper.cmu.edu", event_id: [8]},
        {first_name:"Samuel Philip", last_name:"Ahwesh", email: "spa@tepper.cmu.edu", event_id: [9]},
        {first_name:"Olugbenga", last_name:"Ajala", email: "oajala@tepper.cmu.edu", event_id: [9]},
        {first_name:"Zachary David", last_name:"Bush", email: "zbush@tepper.cmu.edu", event_id: [9]},
        {first_name:"Adam", last_name:"Carlson", email: "adamcarl@tepper.cmu.edu", event_id: [9]},
        {first_name:"Marjorie", last_name:"Stephens Harmon", email: "mharmon@tepper.cmu.edu", event_id: [9]},
        {first_name:"Jeffrey James", last_name:"Klinefelter", email: "jeffk@tepper.cmu.edu", event_id: [9]},
        {first_name:"Amitesh", last_name:"Kumar", email: "amiteshk@tepper.cmu.edu", event_id: [9]},
        {first_name:"Ronak", last_name:"Modi", email: "rmodi@tepper.cmu.edu", event_id: [9]},
        {first_name:"Robert Shannon", last_name:"Mullin", email: "rmullin@tepper.cmu.edu", event_id: [9]},
        {first_name:"Brian Robert", last_name:"Rishel", email: "brishel@tepper.cmu.edu", event_id: [9]},
        {first_name:"Aswin", last_name:"Sairam", email: "aswins@tepper.cmu.edu", event_id: [9]},
        {first_name:"Christian Paul", last_name:"Sirney", email: "csirney@tepper.cmu.edu", event_id: [9]},
        {first_name:"David", last_name:"Villiotti", email: "dvilliot@tepper.cmu.edu", event_id: [9]},
        {first_name:"Tae-Yeon", last_name:"Won", email: "twon@tepper.cmu.edu", event_id: [9]},
        {first_name:"Maeve Pettit", last_name:"Beer", email: "mbeer@tepper.cmu.edu", event_id: [10]},
        {first_name:"Kyle Stephen", last_name:"Bradley", email: "kyleb@tepper.cmu.edu", event_id: [10]},
        {first_name:"Gregory", last_name:"Corbett", email: "gcorbett@tepper.cmu.edu", event_id: [10]},
        {first_name:"Benjamin Brady", last_name:"DeWitt", email: "bdewitt@tepper.cmu.edu", event_id: [10]},
        {first_name:"Anthony Richard", last_name:"Gabbianelli", email: "agabbian@tepper.cmu.edu", event_id: [10]},
        {first_name:"Simone", last_name:"Hollweck", email: "shollwec@tepper.cmu.edu", event_id: [10]},
        {first_name:"Patrick James", last_name:"Lynch", email: "pjl1@tepper.cmu.edu", event_id: [10]},
        {first_name:"Sirshendu", last_name:"Mukherjee", email: "smukherj@tepper.cmu.edu", event_id: [10]},
        {first_name:"Brian Jeffrey", last_name:"Rabbitt", email: "brabbitt@tepper.cmu.edu", event_id: [10]},
        {first_name:"Krishna Chaitanya", last_name:"Samavedam", email: "ksamaved@tepper.cmu.edu", event_id: [10]},
        {first_name:"David", last_name:"Sandora", email: "dsandora@tepper.cmu.edu", event_id: [10]},
        {first_name:"Craig Edward", last_name:"Savolskis", email: "csavolsk@tepper.cmu.edu", event_id: [10]},
        {first_name:"Sarah", last_name:"Tedrow-Azizi", email: "sazizi@tepper.cmu.edu", event_id: [10]},
        {first_name:"Christopher", last_name:"Wright", email: "cwright2@tepper.cmu.edu", event_id: [10]}
      ]
    users.each_with_index { |user_info,index|
      @user = User.create(first_name: "#{user_info[:first_name]}",
                last_name: "#{user_info[:last_name]}",
                role: 'user',
                email: "#{user_info[:email]}",
                password:"improveMeeTest#{index+1}"
                )
      UserSetting.create(user_id: @user.id, program_name:"Focus Group",nickname:"#{user_info[:first_name]}")
      user_info[:event_id].each do |event_id|
        UserEvent.create(user_id:@user.id,event_id:event_id)
      end


    }

  end
end

#Dr09Pc8RChODUzFjdzwCmA