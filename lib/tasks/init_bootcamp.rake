require 'database_cleaner'
namespace :db do
  desc "Clean and fill database with test data for init_summer group"

    task init_tests: :environment do

    User.delete_all
    UserSetting.delete_all
    UserEntity.delete_all
    Entity.delete_all
    FeedbackQuestionScore.delete_all
    Feedback.delete_all
    FeedbackRequest.delete_all
    UserCategory.delete_all
    UserQuestion.delete_all
    UserEvent.delete_all
    Question.delete_all
    Category.delete_all


    #reset pk from database
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean

    @basecamp = Category.create(id:1,name:"Basecamp",description: 'Please provide feedback on the areas that you believe he excelled during basecamp:',overall_score:0)
    Question.create(id:1,category_id:1,text: 'Communication Skills - Delivered his thoughts and opinions in a clear and convincing way',overall_score:0)
    Question.create(id:2,category_id:1,text: 'Leadership - Kept the team focused on the end goal / pushed the team in the right direction',overall_score:0)
    Question.create(id:3,category_id:1,text: 'Teamwork - Made everyone on the team feel heard / brought together team members even when disagreement was present / was engaged and actively contributed to the team’s work',overall_score:0)
    Question.create(id:4,category_id:1,text: 'Social - Brought people together / made others feel welcome',overall_score:0)

    @club = Category.create(id:2,name:"Club",description: 'Share any other thoughts you have about this event with us...What could make this event better?',overall_score:0)
    Question.create(id:5,category_id:2,text: 'Did the event achieve its objective?',overall_score:0)
    Question.create(id:6,category_id:2,text: 'How was the organization of the event? (punctuality, location, promotion, etc)',overall_score:0)
    Question.create(id:7,category_id:2,text: 'Would you recommend this event  to a colleague?',overall_score:0)
    Question.create(id:8,category_id:2,text: 'How useful was the information presented at the event?',overall_score:0)
    Question.create(id:9,category_id:2,text: 'Overall, how would you rate the event?',overall_score:0)


    @user = User.create({password:'qwertyui',role:'organizer',first_name:"Basecamp", last_name:"", email: "basecamp@cmu.edu"})
    UserSetting.create!(user_id: @user.id, program_name:"Basecamp",nickname:"#{@user.first_name}")
    entity = {name:'Basecamp',description:'New CMU students initiation course',entity_type:'class',start_date:Time.new(2014, 05, 27, 18, 00, 0),end_date:Time.new(2014, 05, 27, 19, 45, 0), owner: @user}
    @Entity = Entity.create!(entity)

    EntityCategory.create(entity_id:@Entity, category_id:@basecamp)
    event = {name:'Basecamp',description:'New CMU students initiation course',start_date:Time.new(2014, 05, 27, 18, 00, 0),end_date:Time.new(2014, 05, 27, 19, 45, 0), parent_entity:@Entity}
    @Event = Event.create!(event)

    EventCategory.create(event_id:@Event, category_id:@basecamp)


    @user = User.create({password:'qwertyui',role:'organizer',first_name:"Consulting", last_name:"", email: "consulting@cmu.edu"})
    UserSetting.create!(user_id: @user.id, program_name:"Consulting Group",nickname:"#{@user.first_name}")
    entity = {name:'Basecamp',description:'New CMU students initiation course',entity_type:'class',start_date:Time.new(2014, 05, 27, 18, 00, 0),end_date:Time.new(2014, 05, 27, 19, 45, 0), owner: @user}
    @Entity = Entity.create!(entity)

    EntityCategory.create(entity_id:@Entity, category_id:@club)
    event = {name:'Club',description:'Consulting club event',start_date:Time.new(2014, 05, 27, 18, 00, 0),end_date:Time.new(2014, 05, 27, 19, 45, 0), parent_entity:@Entity}
    @Event = Event.create!(event)

    EventCategory.create(event_id:@Event, category_id:@club)

    users =
        [
            {password:'qwertyui',role:'user',first_name:"Carlos", last_name:"Crespo", email: "carlos@tepper.cmu.edu"},
            {password:'qwertyui',role:'user',first_name:"Cristian", last_name:"Caroli", email: "cristian@tepper.cmu.edu"},
            {password:'qwertyui',role:'user',first_name:"Jose", last_name:"Dunia", email: "jose@tepper.cmu.edu"},
            {password:'qwertyui',role:'user',first_name:"Moises", last_name:"Morgenstern", email: "Moises@tepper.cmu.edu"},
            {password:'qwertyui',role:'user',first_name:"Ignacio", last_name:"Morales", email: "ignacio@tepper.cmu.edu"}
        ]
    users.each_with_index { |user_info,index|
      user = User.create(user_info)
      UserSetting.create!(user_id: user.id, program_name:"CMU",nickname:"#{user.first_name}")
      Entity.all.each { |e| UserEntity.create!(user_id:user.id, entity_id:e.id,admin:false) }
      Event.all.each { |e| UserEvent.create!(user_id:user.id,event_id:e.id) }
    }

    AppSetting.create!(sends_email:true);
    #

    #feedback_requests = [
    #    { solicitor_id: User.find(2).id, solicited_id:User.find(3).id, event_id:Event.first.id,reply:false },
    #    { solicitor_id: User.find(2).id, solicited_id:User.find(4).id, event_id:Event.first.id,reply:false },
    #    { solicitor_id: User.find(2).id, solicited_id:User.find(5).id, event_id:Event.first.id,reply:false },
    #    { solicitor_id: User.find(3).id, solicited_id:User.find(6).id, event_id:Event.first.id,reply:false },
    #    { solicitor_id: User.find(4).id, solicited_id:User.find(6).id, event_id:Event.first.id,reply:false }
    #]
    #
    #feedback_requests.each do |feedback_request|
    #    FeedbackRequest.create!(feedback_request)
    #end

    #feedbacks = [
    #    { sender_id: User.find(6).id, receiver_id: User.find(5).id, event_id: Event.first.id, is_anonymous: false, comments: 'comments', seen: false, category_id: 1},
    #    { sender_id: User.find(6).id, receiver_id: User.find(2).id, event_id: Event.first.id, is_anonymous: false, comments: 'comments', seen: false, category_id: 1},
    #    { sender_id: User.find(3).id, receiver_id: User.find(4).id, event_id: Event.first.id, is_anonymous: false, comments: 'comments', seen: false, category_id: 1},
    #    { sender_id: User.find(4).id, receiver_id: User.find(3).id, event_id: Event.first.id, is_anonymous: false, comments: 'comments', seen: false, category_id: 1},
    #    { sender_id: User.find(5).id, receiver_id: User.find(3).id, event_id: Event.first.id, is_anonymous: false, comments: 'comments', seen: false, category_id: 1}
    #]
    #
    #
    #prng = Random.new
    #feedbacks.each do |feedback_info|
    #  @feedback = Feedback.create(feedback_info)
    #  Question.where(category_id:Category.first.id).each do |question|
    #
    #    FeedbackQuestionScore.create!(feedback_id:@feedback.id,question_id:question.id,score: prng.rand(1..5))
    #
    #  end
    #end


    entity = {name:'Consulting Group',description:'Consulting group',entity_type:'club',start_date:Time.new(2014, 05, 27, 18, 00, 0),end_date:Time.new(2014, 05, 27, 19, 45, 0), owner: u}
    end
end