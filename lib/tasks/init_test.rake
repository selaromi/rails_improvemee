namespace :db do
  desc "Clean and fill database with test data"

  task init_test: :environment do

    User.delete_all
    UserSetting.delete_all
    FeedbackQuestionScore.delete_all
    Feedback.delete_all
    FeedbackRequest.delete_all
    UserCategory.delete_all
    UserQuestion.delete_all
    UserEvent.delete_all

    (1..5).each { |index|
      @user = User.create(first_name: "User #{index}",
                last_name: "Lastname #{index}",
                role: 'user',
                email: "testuser#{index}@cmu.edu",
                password:"testuser"
                )
      UserSetting.new(user_id: @user.id, program_name:"Test",nickname:"little #{index}")
      UserEvent.create(user_id:@user.id,event_id:1)

    }

    UserEvent
  end
end