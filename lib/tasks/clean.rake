namespace :db do
  desc "Clean and fill database with sample data"

  task clean: :environment do

    Category.delete_all
    Question.delete_all
    Event.delete_all

    Category.create(id:1,name:"Recruiting Performance",description:"Recruiting Performance",overall_score:0)
    Question.create(id:1,category_id:1,text:"¿RP Question 1?",overall_score:0)
    Question.create(id:2,category_id:1,text:"¿RP Question 2?",overall_score:0)
    Question.create(id:3,category_id:1,text:"¿RP Question 3?",overall_score:0)
    Question.create(id:4,category_id:1,text:"¿RP Question 4?",overall_score:0)
    Question.create(id:5,category_id:1,text:"¿RP Question 5?",overall_score:0)
    Category.create(id:2,name:"Perception",description:"Perception",overall_score:0)
    Question.create(id:6,category_id:2,text:"¿Pe Question 1?",overall_score:0)
    Question.create(id:7,category_id:2,text:"¿Pe Question 2?",overall_score:0)
    Question.create(id:8,category_id:2,text:"¿Pe Question 3?",overall_score:0)
    Question.create(id:9,category_id:2,text:"¿Pe Question 4?",overall_score:0)
    Question.create(id:10,category_id:2,text:"¿Pe Question 5?",overall_score:0)
    Category.create(id:3,name:"Presentations",description:"Presentations",overall_score:0)
    Question.create(id:11,category_id:3,text:"¿Pr Question 1?",overall_score:0)
    Question.create(id:12,category_id:3,text:"¿Pr Question 2?",overall_score:0)
    Question.create(id:13,category_id:3,text:"¿Pr Question 3?",overall_score:0)
    Question.create(id:14,category_id:3,text:"¿Pr Question 4?",overall_score:0)
    Question.create(id:15,category_id:3,text:"¿Pr Question 5?",overall_score:0)
    Category.create(id:4,name:"Team Performance",description:"Team Performance",overall_score:0)
    Question.create(id:16,category_id:4,text:"¿TP Question 1?",overall_score:0)
    Question.create(id:17,category_id:4,text:"¿TP Question 2?",overall_score:0)
    Question.create(id:18,category_id:4,text:"¿TP Question 3?",overall_score:0)
    Question.create(id:19,category_id:4,text:"¿TP Question 4?",overall_score:0)
    Question.create(id:20,category_id:4,text:"¿TP Question 5?",overall_score:0)

  end
end